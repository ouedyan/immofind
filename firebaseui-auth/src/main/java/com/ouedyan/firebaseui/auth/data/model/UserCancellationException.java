package com.ouedyan.firebaseui.auth.data.model;

import com.ouedyan.firebaseui.auth.ErrorCodes;
import com.ouedyan.firebaseui.auth.FirebaseUiException;

import androidx.annotation.RestrictTo;

@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
public class UserCancellationException extends FirebaseUiException {
    public UserCancellationException() {
        super(ErrorCodes.UNKNOWN_ERROR);
    }
}

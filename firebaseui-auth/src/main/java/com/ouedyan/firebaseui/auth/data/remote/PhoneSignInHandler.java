package com.ouedyan.firebaseui.auth.data.remote;

import android.app.Application;
import android.content.Intent;

import com.ouedyan.firebaseui.auth.AuthUI;
import com.ouedyan.firebaseui.auth.IdpResponse;
import com.ouedyan.firebaseui.auth.data.model.Resource;
import com.ouedyan.firebaseui.auth.data.model.UserCancellationException;
import com.ouedyan.firebaseui.auth.ui.HelperActivityBase;
import com.ouedyan.firebaseui.auth.ui.phone.PhoneActivity;
import com.ouedyan.firebaseui.auth.viewmodel.ProviderSignInBase;
import com.ouedyan.firebaseui.auth.viewmodel.RequestCodes;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;

@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
public class PhoneSignInHandler extends ProviderSignInBase<AuthUI.IdpConfig> {
    public PhoneSignInHandler(Application application) {
        super(application);
    }

    @Override
    public void startSignIn(@NonNull HelperActivityBase activity) {
        activity.startActivityForResult(
                PhoneActivity.createIntent(
                        activity, activity.getFlowParams(), getArguments().getParams()),
                RequestCodes.PHONE_FLOW);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RequestCodes.PHONE_FLOW) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (response == null) {
                setResult(Resource.<IdpResponse>forFailure(new UserCancellationException()));
            } else {
                setResult(Resource.forSuccess(response));
            }
        }
    }

    @Override
    public void startSignIn(@NonNull FirebaseAuth auth,
                            @NonNull HelperActivityBase activity,
                            @NonNull String providerId) {
        startSignIn(activity);
    }
}

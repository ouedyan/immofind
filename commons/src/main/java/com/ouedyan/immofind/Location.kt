package com.ouedyan.immofind

import com.ouedyan.immofind.model.City
import com.ouedyan.immofind.model.Country
import java.util.*
import kotlin.collections.ArrayList

//TODO Replace all Arraylists by Lists where Arraylists not necessary

object Location {
    //TODO Get from settings (choose Country first then locale from this country's locales)
    const val currentLocale = "fr_BF"

    val countries: ArrayList<Country> = arrayListOf(
        Country(
            "BF", arrayListOf(Locale("fr", "BF")), arrayListOf(
                City(
                    "Ouagadougou", countryCode = "BF", numberOfSectors = 55, neighborhoods = arrayListOf(
                        "1200 Logements",
                        "Bilbalogho",
                        "Cissin",
                        "Cité An II",
                        "Cité An III",
                        "Cité An IV B",
                        "Cité Azimmo Ouaga 2000",
                        "Cité Azimmo Tampouy",
                        "Cité de l´Avenir",
                        "Dagnoen",
                        "Dapoya",
                        "Dassasgo",
                        "Goughin",
                        "Hamdalaye",
                        "Kalgondin",
                        "Kamsonghin",
                        "Kilwin",
                        "Kologh-Naba",
                        "Kossodo",
                        "Koulouba",
                        "Koumdanyoré",
                        "Larlé",
                        "Nemnin",
                        "Nonsin",
                        "Ouidi",
                        "Paspanga",
                        "Patte d´oie",
                        "Petit Paris",
                        "Pissy",
                        "Saint Léon",
                        "Samandin",
                        "Sanyiri",
                        "Somgandé",
                        "Tabtenga",
                        "Tanghin",
                        "Wayalguin",
                        "Wemtenga",
                        "Zogona",
                        "Zone du Bois",
                        "ZAD",
                        "Zone I",
                        "Zone Industriel de Kossodo",
                        "Zone Industrielle de Gounghin"
                    )
                ),
                City(
                    "Bobo-Dioulasso", countryCode = "BF", neighborhoods = arrayListOf(
                        "Accart-Ville",
                        "Bindougousso",
                        "Bolomakoté",
                        "cour Daouda et Abdoulaye",
                        "Dogona",
                        "Koko",
                        "Kuinima",
                        "Lafiabougou",
                        "Ouezzin Ville",
                        "Petit Paris",
                        "Saint Etienne",
                        "Samogan",
                        "Sarfalao",
                        "Sikasso Sira",
                        "Sonsoribougou",
                        "Tounouma",
                        "Zone Industrielle",
                        "Zone Residentielle B"
                    )
                )
            )
        )
    )
        get() = ArrayList(field)

    fun getCountry(code: String): Country? {
        countries.forEach {
            if (it.code.uppercase() == code.uppercase()) return@getCountry it
        }
        return null
    }

    /**
     * Get list of bf cities in most populated order
     *
     * @return list of Bf cities
     */
    val bfCitiesList: ArrayList<String> = arrayListOf(
        "Ouagadougou",
        "Bobo-Dioulasso",
        "Koudougou",
        "Banfora",
        "Ouahigouya",
        "Dédougou",
        "Pissila",
        "Kaya",
        "Tanghin-Dassouri",
        "Tenkodogo",
        "Pouytenga",
        "Fada N'Gourma",
        "Kordié",
        "Ouargaye",
        "Garango",
        "Dori",
        "Kongoussi",
        "Kokologo",
        "Réo",
        "Diapaga",
        "Houndé",
        "Kouka",
        "Yako",
        "Djibo",
        "Khyon",
        "Léo",
        "Nouna",
        "Pô",
        "Koupéla",
        "Zorgho",
        "Kombissiri",
        "Bonou-Toaga",
        "Orodara",
        "Gourcy",
        "Tougan",
        "Boulsa",
        "Titao",
        "Manga",
        "Pama",
        "Boromo",
        "Diébougou",
        "Ziniaré",
        "Dano",
        "Boussé",
        "Solenzo"
    )
        get() = ArrayList(field)
    
}
package com.ouedyan.immofind

import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView

abstract class SelectableItemRecyclerViewAdapter<V : RecyclerView.ViewHolder> : RecyclerView.Adapter<V>() {
    
    var onItemClickListener: OnItemClickListener? = null
    var onItemSelectListener: OnItemSelectListener? = null
    
    var isSelectable: Boolean = false
    
    private val selectedItemsInternal = HashSet<Int>()
    
    enum class OnBindViewHolderSelectPayLoad {
        ON_CLICK,
        ON_LONG_CLICK,
        ON_SELECT
    }
    
    /***
     * Returns a clone list of selected items
     */
    val selectedItems
        get() = HashSet(selectedItemsInternal)
    
    interface OnItemSelectListener {
        /**
         * Called when an item is selected or deselected
         */
        fun onItemSelect(position: Int, itemView: View, cancellation: Boolean)
        
        /**
         * Called to set item's layout according to it being selected or not.
         * Will be also called at each [onBindViewHolder]
         */
        fun onItemSelectViewState(position: Int, itemView: View, cancellation: Boolean)
    }
    
    interface OnItemClickListener {
        fun onItemClick(position: Int, itemView: View)
        fun onLongClick(position: Int, itemView: View)
    }
    
    @CallSuper
    override fun onBindViewHolder(holder: V, position: Int) {
        
        holder.itemView.setOnClickListener { view ->
            onItemClickInternal(position, view)
            onItemClickListener?.onItemClick(position, view)
        }
        holder.itemView.setOnLongClickListener { view ->
            onItemLongClickInternal(position, view)
            onItemClickListener?.onLongClick(position, view)
            true
        }
        
        onItemSelectListener?.onItemSelectViewState(position, holder.itemView, !selectedItemsInternal.contains(position))
        
    }
    
    @CallSuper
    override fun onBindViewHolder(holder: V, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNotEmpty()) {
            if (payloads.contains(OnBindViewHolderSelectPayLoad.ON_SELECT)) {
                onItemSelectListener?.onItemSelectViewState(position, holder.itemView, !selectedItemsInternal.contains(position))
                
            }
        } else super.onBindViewHolder(holder, position, payloads)
    }
    
    private fun onItemClickInternal(position: Int, itemView: View) {
        if (isSelectable) {
            if (selectedItemsInternal.isNotEmpty()) {
                if (selectedItemsInternal.contains(position)) {
                    unselectItemInternal(position, itemView)
                } else {
                    selectItemInternal(position, itemView)
                }
            }
        }
    }
    
    private fun onItemLongClickInternal(position: Int, itemView: View) {
        if (isSelectable) {
            if (selectedItemsInternal.isEmpty()) {
                selectItemInternal(position, itemView)
            }
        }
        
    }
    
    private fun selectItemInternal(position: Int, itemView: View? = null) {
        if (isSelectable && !selectedItemsInternal.contains(position)) {
            selectedItemsInternal.add(position)
            notifyItemChanged(position, OnBindViewHolderSelectPayLoad.ON_SELECT)
            if (itemView != null) onItemSelectListener?.onItemSelect(position, itemView, false)
        }
    }
    
    /**
     *
     * Note : Doesn't trigger OnItemSelectedListener
     */
    fun selectItem(position: Int) {
        selectItemInternal(position)
    }
    
    private fun unselectItemInternal(position: Int, itemView: View? = null) {
        if (isSelectable && selectedItemsInternal.contains(position)) {
            selectedItemsInternal.remove(position)
            notifyItemChanged(position, OnBindViewHolderSelectPayLoad.ON_SELECT)
            if (itemView != null) onItemSelectListener?.onItemSelect(position, itemView, true)
        }
        
    }
    
    
    /**
     *
     * Note : Doesn't trigger OnItemSelectedListener
     */
    fun unselectItem(position: Int) {
        unselectItemInternal(position)
    }
    
    fun clearSelection() {
        selectedItemsInternal.toList().forEach { position ->
            unselectItemInternal(position)
        }
    }
    
    fun selectAllItems() {
        (0 until itemCount).forEach { position ->
            selectItemInternal(position)
        }
    }
    
}
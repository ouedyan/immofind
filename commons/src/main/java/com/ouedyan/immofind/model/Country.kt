package com.ouedyan.immofind.model

import java.util.*
import kotlin.collections.ArrayList

class Country(val code: String, val locales: ArrayList<Locale>, val cities: ArrayList<City>) {

    fun getCity(cityCode: String): City? {
        cities.forEach {
            if (it.code == cityCode) return@getCity it
        }
        return null
    }
}
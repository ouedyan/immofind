package com.ouedyan.immofind.model

import android.content.Context
import androidx.annotation.StringRes
import com.google.firebase.firestore.*
import com.ouedyan.immofind.Location
import com.ouedyan.immofind.R
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

@IgnoreExtraProperties
class Property {
    @DocumentId
    lateinit var uid: String
    lateinit var ownerUid: String

    lateinit var status : PropertyStatus
    lateinit var state : State

    //lateinit var typeOfPropertyData: TypeOfPropertyData

    //Main image at 0
    var propertyPicturesUrls = ArrayList<String>()
    var tags = ArrayList<Tag>()

    @ServerTimestamp
    var postDate: Date? = null
    lateinit var cityCode: String
    lateinit var locationInCity: String
    lateinit var shortDescription: String
    lateinit var htmlDetailedDescription: String
    var rentIntervalUnit: RentIntervalUnit = RentIntervalUnit.MONTH
    var rentInterval: Int = 1
    lateinit var locale: String
    var rentPrice: Double? = null
    var salePrice: Double? = null

    //Showed area, either surface either land surface
    var areaUnit: AreaUnit? = null
    var area: Double? = null

    val propertyMainPictureUrl: String?
        get() = try {
            propertyPicturesUrls[0]
        } catch (e: IndexOutOfBoundsException) {
            null
        }

    enum class RentIntervalUnit  //Modifier 'private' is redundant for enum constructors
        (@field:StringRes @param:StringRes val stringRes: Int) {
        HOUR(R.string.hour),
        DAY(R.string.day),
        WEEK(R.string.week),
        MONTH(R.string.month),
        TRIMESTER(R.string.trimester),
        SEMESTER(R.string.semester),
        YEAR(R.string.year);
    }

    enum class AreaUnit(@field:StringRes @param:StringRes val stringRes: Int) {
        SQUARE_METERS(R.string.square_meters),
        HECTARES(R.string.hectares);
    }

    enum class PropertyStatus(@field:StringRes @param:StringRes val stringRes: Int) {
        FOR_RENT(R.string.for_rent),
        FOR_SALE(R.string.for_sale)
    }

    enum class TypeOfProperty(
        @field:StringRes @param:StringRes val stringRes: Int,
        val typeofPropertyDetailsClass: Class<out TypeOfPropertyDetails>? = null
    ) {
        HABITATION(R.string.habitation, HabitationDetails::class.java),
        LAND_OR_LAND_LOT(R.string.land_or_land_lot, LandOrFarmDetails::class.java),
        BUILDING(R.string.building, BuildingDetails::class.java),
        COMMERCIAL_PLACE(R.string.commercial_place),
        OFFICE_ROOMS(R.string.office_rooms, OfficeRoomsDetails::class.java),
        STORE(R.string.store),
        FARM(R.string.farm, LandOrFarmDetails::class.java),
        FACTORY(R.string.factory);
    }
    
    enum class State(@field:StringRes @param:StringRes val stringRes: Int) {
        AVAILABLE(R.string.available),
        UNAVAILABLE(R.string.unavailable),
        ACQUIRED(R.string.acquired)
    }


    sealed class TypeOfPropertyData(
        val typeOfProperty: TypeOfProperty,
        val details: TypeOfPropertyDetails? = null
    ) {

        class Habitation(details: HabitationDetails) :
            TypeOfPropertyData(TypeOfProperty.HABITATION, details)

        class LandOrLandLot(details: LandOrFarmDetails) :
            TypeOfPropertyData(TypeOfProperty.LAND_OR_LAND_LOT, details)

        class Building(details: BuildingDetails) :
            TypeOfPropertyData(TypeOfProperty.BUILDING, details)

        class OfficeRooms(details: OfficeRoomsDetails) :
            TypeOfPropertyData(TypeOfProperty.OFFICE_ROOMS, details)

        class Farm(details: LandOrFarmDetails) :
            TypeOfPropertyData(TypeOfProperty.FARM, details)

        class CommercialPlace : TypeOfPropertyData(TypeOfProperty.COMMERCIAL_PLACE)
        class Store : TypeOfPropertyData(TypeOfProperty.STORE)
        class Factory : TypeOfPropertyData(TypeOfProperty.FACTORY)

    }

    enum class HabitationSpecificity(@field:StringRes @param:StringRes val stringRes: Int) {
        FURNISHED(R.string.furnished),
        ACCESS_TO_ELECTRICITY(R.string.access_to_electricity),
        ACCESS_TO_WATER(R.string.access_to_water),
        AIR_CONDITIONED(R.string.air_conditioned),
        FENCED(R.string.fenced),
        GARAGES(R.string.garages),
        SWIMMING_POOLS(R.string.swimming_pools)
    }

    enum class LandOrFarmSpecificity(@field:StringRes @param:StringRes val stringRes: Int) {
        ACCESS_TO_ELECTRICITY(R.string.access_to_electricity),
        ACCESS_TO_WATER(R.string.access_to_water),
        FENCED(R.string.fenced)
    }

    enum class BuildingSpecificity(@field:StringRes @param:StringRes val stringRes: Int) {
        FENCED(R.string.fenced),
        PARKING(R.string.parking),
        GARAGES(R.string.garages)
    }

    abstract class TypeOfPropertySpecificityData(val qty: Int = -1)

    sealed class HabitationSpecificityData(
        val habitationSpecificity: HabitationSpecificity,
        qty: Int = -1
    ) : TypeOfPropertySpecificityData(qty) {
        class Furnished : HabitationSpecificityData(HabitationSpecificity.FURNISHED)
        class AccessToElectricity : HabitationSpecificityData(HabitationSpecificity.ACCESS_TO_ELECTRICITY)
        class AccessToWater : HabitationSpecificityData(HabitationSpecificity.ACCESS_TO_WATER)
        class AirConditioned : HabitationSpecificityData(HabitationSpecificity.AIR_CONDITIONED)
        class Fenced : HabitationSpecificityData(HabitationSpecificity.FENCED)
        class Garages(qty: Int = 0) : HabitationSpecificityData(HabitationSpecificity.GARAGES, qty)
        class SwimmingPools(qty: Int = 0) : HabitationSpecificityData(HabitationSpecificity.SWIMMING_POOLS, qty)
    }

    sealed class LandOrFarmSpecificityData(
        val landOrFarmSpecificity: LandOrFarmSpecificity,
        qty: Int = -1
    ) : TypeOfPropertySpecificityData(qty) {
        class AccessToElectricity : LandOrFarmSpecificityData(LandOrFarmSpecificity.ACCESS_TO_ELECTRICITY)
        class AccessToWater : LandOrFarmSpecificityData(LandOrFarmSpecificity.ACCESS_TO_WATER)
        class Fenced : LandOrFarmSpecificityData(LandOrFarmSpecificity.FENCED)
    }

    sealed class BuildingSpecificityData(
        val buildingSpecificity: BuildingSpecificity,
        qty: Int = -1
    ) : TypeOfPropertySpecificityData(qty) {
        class Fenced : BuildingSpecificityData(BuildingSpecificity.FENCED)
        class Parking : BuildingSpecificityData(BuildingSpecificity.PARKING)
        class Garages(qty: Int = 0) : BuildingSpecificityData(BuildingSpecificity.GARAGES, qty)
    }

    abstract class TypeOfPropertyDetails {
        abstract val typesOfProperty: List<TypeOfProperty>

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || this::class.qualifiedName != other::class.qualifiedName) return false
            return typesOfProperty == (other as TypeOfPropertyDetails).typesOfProperty
        }

        override fun hashCode(): Int {
            return typesOfProperty.hashCode()
        }

    }

    abstract class HabitationDetails : TypeOfPropertyDetails() {
        final override val typesOfProperty = listOf(TypeOfProperty.HABITATION)
        abstract val surface: Double
        abstract val landSurface: Double
        abstract val numberOfRooms: Int
        abstract val numberOfBedrooms: Int
        abstract val specificity: List<HabitationSpecificityData>
    }

    abstract class LandOrFarmDetails : TypeOfPropertyDetails() {
        final override val typesOfProperty = listOf(TypeOfProperty.LAND_OR_LAND_LOT, TypeOfProperty.FARM)
        abstract val landSurface: Double
        abstract val specificity: List<LandOrFarmSpecificityData>
    }

    abstract class BuildingDetails : TypeOfPropertyDetails() {
        final override val typesOfProperty = listOf(TypeOfProperty.BUILDING)
        abstract val surface: Double
        abstract val numberOfRooms: Int
        abstract val specificity: List<BuildingSpecificityData>
    }

    abstract class OfficeRoomsDetails : TypeOfPropertyDetails() {
        final override val typesOfProperty = listOf(TypeOfProperty.OFFICE_ROOMS)
        abstract val numberOfOfficeRooms: Int
    }


    fun getAreaWithUnit(context: Context): String? {
        var result : String? = null
        area?.let {
            val numberFormatter = NumberFormat.getInstance()
            numberFormatter.maximumFractionDigits = it.toBigDecimal().stripTrailingZeros().scale()
            if (numberFormatter.maximumFractionDigits > 0) numberFormatter.maximumFractionDigits = 4

            result = numberFormatter.format(area) + " " + context.getString(
                areaUnit!!.stringRes
            )
        }

        return result
    }

    @Exclude
    fun getCity(): City {
        return Location.getCountry(cityCode.substringBefore('_'))!!.getCity(cityCode)!!
    }

    @Exclude
    fun getCountry(): Country {
        return Location.getCountry(cityCode.substringBefore('_'))!!
    }

    /**
     * Get the string containing both sale/rent price along with
     * rent interval of the given property if it is for rent.
     *
     * @param context  Context from which to get the Resource Manager to
     * retrieve the translated strings accordingly
     * @return the assembled string
     * @see .getRentPrice
     * @see .getSalePrice
     * @see .getRentIntervalUnit
     */
    fun getPropertyFormattedPrice(context: Context): String {
        val numberFormatter = NumberFormat.getCurrencyInstance(Locale(locale.substringBefore('_'), locale.substringAfter('_')))
        return if (status == PropertyStatus.FOR_RENT) {
            rentPrice?.let {
                numberFormatter.maximumFractionDigits = it.toBigDecimal().stripTrailingZeros().scale()
                if (numberFormatter.maximumFractionDigits > 0) numberFormatter.apply {
                    minimumFractionDigits = 2
                    maximumFractionDigits = 3
                }
                numberFormatter.format(it)
            }.toString() +
                    " / " + context.getString(rentIntervalUnit.stringRes)
        } else  //For sale
            salePrice?.let {
                numberFormatter.maximumFractionDigits = it.toBigDecimal().stripTrailingZeros().scale()
                numberFormatter.format(it)
            }.toString()

    }
}
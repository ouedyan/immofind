package com.ouedyan.immofind.model

import android.content.Context
import android.content.res.Resources
import androidx.annotation.StringRes


class Tag @JvmOverloads constructor(
    /**
     * Tag's text's string resource entry name as specified by [Resources.getResourceEntryName]
     */
    val textStringResourceEntryName: String? = null,
    quantity: Int = -1,
    type: TAG_TYPE? = TAG_TYPE.DESCRIPTION_TAG
) {
    
    var type = type
        set(value) {
            if (value == TAG_TYPE.PROPERTY_TYPE_TAG) isTextOnlyTag = true
            field = value
        }
    
    var quantity = quantity
        set(value) {
            isTextOnlyTag = (value < 0)
            field = value
        }
    
    var isTextOnlyTag = true
    
    enum class TAG_TYPE {
        PROPERTY_TYPE_TAG,
        DESCRIPTION_TAG
    }
    
    init {
        this.quantity = quantity
    }
    
    //textString nullable For Firestore (empty constructor needed)
    
}
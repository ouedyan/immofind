package com.ouedyan.immofind.model

class City(
    val name: String,
    val countryCode : String,
    var numberOfSectors: Int? = null,
    var neighborhoods: ArrayList<String>? = null
){
    val code = countryCode + '_' + name.hashCode()

}
package com.ouedyan.immofind.ui

import android.content.Context
import android.net.Uri
import android.text.style.URLSpan
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorInt
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import com.ouedyan.immofind.R
import java.lang.ref.WeakReference

class CustomTabSpan(context: Context, url: String) : URLSpan(url) {
    private val mContext: WeakReference<Context> = WeakReference(context)
    private val mUrl: String = url
    private val mCustomTabsIntent: CustomTabsIntent
    
    override fun onClick(widget: View) {
        val context = mContext.get()
        if (context != null) {
            mCustomTabsIntent.launchUrl(context, Uri.parse(mUrl))
        }
    }

    init {
        // Getting default color
        val typedValue = TypedValue()
        context.theme.resolveAttribute(R.attr.colorPrimary, typedValue, true)
        @ColorInt val color = typedValue.data
        mCustomTabsIntent = CustomTabsIntent.Builder()
                .setDefaultColorSchemeParams(CustomTabColorSchemeParams.Builder()
                        .setToolbarColor(color)
                        .build())
                .setShowTitle(true)
                .build()
    }
}
package com.ouedyan.immofind.ui

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.*
import androidx.core.widget.TextViewCompat
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.JustifyContent
import com.ouedyan.immofind.R
import com.ouedyan.immofind.Utils.dpToPx
import java.math.BigDecimal
import it.sephiroth.android.library.numberpicker.NumberPicker
import it.sephiroth.android.library.numberpicker.doOnProgressChanged


class MultiQuantifiedToggleLayout
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    FlexboxLayout(context, attrs, defStyleAttr) {

    private val mContext = context

    private val stringIdsMap = HashMap<String, Int>()

    private val extensionListButton = ToggleButton(context)

    private val extensionListPopupLayout = LayoutInflater.from(context).inflate(
        R.layout
            .multi_quantified_toggle_layout_extension_popup_layout, null
    )

    private val extensionListPopupWindow = PopupWindow(
        extensionListPopupLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams
            .MATCH_PARENT, true
    )

    private val extensionFlexBoxLayout = extensionListPopupLayout.findViewById<FlexboxLayout>(
        R.id.extension_flexbox_layout
    )

    var togglesCount = 0
        get() {
            var count = 0
            forEach {
                if (it is QuantifiedToggleButton && getStringId(it) != null)
                    ++count
            }
            return count
        }
        private set

    var isExtensionListEnabled = true
        set(enabled) {
            //TODO Refresh main list and extension list
            if (enabled) {
                if (togglesCount > mainListSize) {
                    extensionListButton.visibility = VISIBLE
                    val togglesSurplusCount = togglesCount - mainListSize
                    for (i in togglesCount - togglesSurplusCount - 1 until togglesCount) {
                        val currentToggle = this[i]
                        if (currentToggle is QuantifiedToggleButton) {
                            val flexLayoutParams = currentToggle.layoutParams as LayoutParams
                            super.removeView(currentToggle)
                            extensionFlexBoxLayout.addView(currentToggle, flexLayoutParams)

                        }
                    }
                }
                updateExtensionListButtonState()
            } else {
                extensionListButton.visibility = GONE
                if (extensionFlexBoxLayout.size > 0) {
                    extensionFlexBoxLayout.forEach {
                        it as QuantifiedToggleButton
                        val flexLayoutParams = it.layoutParams as LayoutParams
                        extensionFlexBoxLayout.removeView(it)
                        super.addView(it, -1, flexLayoutParams)
                    }
                }
            }
            field = enabled
        }

    var mainListSize = 8
        set(value) {
            if (isExtensionListEnabled) {
                if (togglesCount > value) {
                    extensionListButton.visibility = VISIBLE
                    val togglesSurplusCount = togglesCount - mainListSize
                    for (i in togglesCount - togglesSurplusCount - 1 until togglesCount) {
                        val currentToggle = this[i]
                        if (currentToggle is QuantifiedToggleButton) {
                            val flexLayoutParams = currentToggle.layoutParams as LayoutParams
                            super.removeView(currentToggle)
                            extensionFlexBoxLayout.addView(currentToggle, flexLayoutParams)

                        }
                    }

                } else if (togglesCount < value) {
                    var holesCount = value - togglesCount
                    holesCount = holesCount.coerceAtMost(extensionFlexBoxLayout.size)
                    for (i in 0 until holesCount) {
                        val currentToggle = extensionFlexBoxLayout[i]
                        if (currentToggle is QuantifiedToggleButton) {
                            val flexLayoutParams = currentToggle.layoutParams as LayoutParams
                            extensionFlexBoxLayout.removeView(currentToggle)
                            super.addView(currentToggle, -1, flexLayoutParams)

                        }
                    }
                }
                updateExtensionListButtonState()
            }
            field = value
        }

    var extensionListButtonText = "More..."
        set(value) {
            extensionListButton.textOn = value
            extensionListButton.textOff = value
            field = value
        }

    var extensionListScreenTitle = "Others"
        set(value) {
            extensionListPopupLayout.findViewById<TextView>(R.id.title).text = value
            field = value
        }

    var extensionListButtonBackgroundSelector: Drawable? = ResourcesCompat.getDrawable(
        context.resources, R.drawable
            .multi_quantified_toggle_layout_extension_list_button_background_selector, context.theme
    )
        set(value) {
            extensionListButton.background = value?.constantState?.newDrawable()
            field = value
        }

    var extensionListButtonTextColorStateList: ColorStateList? = ResourcesCompat.getColorStateList(
        context.resources, R.color
            .multi_quantified_toggle_layout_extension_list_button_text_color_selector, context.theme
    )
        set(value) {
            extensionListButton.setTextColor(value)
            field = value
        }

    var extensionListButtonTextAppearance: Int = R.style.TextAppearance_AppCompat_Body2
        set(value) {
            TextViewCompat.setTextAppearance(extensionListButton, value)
            field = value
        }


    var togglesBackgroundSelector: Drawable? =
        ResourcesCompat.getDrawable(context.resources, R.drawable.toggle_button_background_selector, context.theme)
        set(value) {
            forEach {
                if (it is QuantifiedToggleButton)
                    it.toggleButton.background = value?.constantState?.newDrawable()
            }

            extensionFlexBoxLayout.forEach {
                if (it is QuantifiedToggleButton)
                    it.toggleButton.background = value?.constantState?.newDrawable()
            }

            field = value
        }

    var qtyBadgesBackground = ResourcesCompat.getDrawable(context.resources, R.drawable.qty_badge_background, context.theme)
        set(value) {
            forEach {
                if (it is QuantifiedToggleButton)
                    it.qtyBadge.background = value
            }

            extensionFlexBoxLayout.forEach {
                if (it is QuantifiedToggleButton)
                    it.qtyBadge.background = value
            }

            field = value
        }

    var togglesTextColorStateList: ColorStateList? = ResourcesCompat.getColorStateList(
        context.resources, R.color
            .text_radio_toggle_color_selector, context.theme
    )
        set(value) {
            forEach {
                if (it is QuantifiedToggleButton)
                    it.toggleButton.setTextColor(value)
            }

            extensionFlexBoxLayout.forEach {
                if (it is QuantifiedToggleButton)
                    it.toggleButton.setTextColor(value)
            }

            field = value
        }

    var qtyBadgesTextColor = ResourcesCompat.getColor(context.resources, android.R.color.white, context.theme)
        set(value) {
            forEach {
                if (it is QuantifiedToggleButton)
                    it.qtyBadge.setTextColor(value)
            }

            extensionFlexBoxLayout.forEach {
                if (it is QuantifiedToggleButton)
                    it.qtyBadge.setTextColor(value)
            }

            field = value
        }

    var togglesTextAppearance: Int = R.style.TextAppearance_AppCompat_Body2
        set(value) {
            forEach {
                if (it is QuantifiedToggleButton)
                    TextViewCompat.setTextAppearance(it.toggleButton, value)
            }

            extensionFlexBoxLayout.forEach {
                if (it is QuantifiedToggleButton)
                    TextViewCompat.setTextAppearance(it.toggleButton, value)
            }

            field = value
        }

    var qtyBadgesTextAppearance: Int = R.style.QuantifiedToggleButtonQtyBadgeTextAppearance
        set(value) {
            forEach {
                if (it is QuantifiedToggleButton)
                    TextViewCompat.setTextAppearance(it.qtyBadge, value)
            }

            extensionFlexBoxLayout.forEach {
                if (it is QuantifiedToggleButton)
                    TextViewCompat.setTextAppearance(it.qtyBadge, value)
            }

            field = value
        }

    var togglesFlexBasisPercent: Float = 0.3333333333f
        set(value) {
            if (value !in 0f..1f) throw IllegalArgumentException("flexBasisPercent must be between 0 and 1")

            forEach {
                val flexLayoutParam = it.layoutParams as LayoutParams
                flexLayoutParam.flexBasisPercent = value
                it.layoutParams = flexLayoutParam
            }

            extensionFlexBoxLayout.forEach {
                val flexLayoutParam = it.layoutParams as LayoutParams
                flexLayoutParam.flexBasisPercent = value
                it.layoutParams = flexLayoutParam
            }

            extensionListButton.layoutParams?.let {
                val flexLayoutParam = it as LayoutParams
                flexLayoutParam.flexBasisPercent = value
                extensionListButton.layoutParams = flexLayoutParam
            }

            field = value
        }

    init {
        //Defaults for flexBox layout.. Customizable
        flexWrap = FlexWrap.WRAP
        flexDirection = FlexDirection.ROW
        justifyContent = JustifyContent.FLEX_START

        //Extension List Default Configuration
        extensionListButton.apply {
            background = extensionListButtonBackgroundSelector?.constantState?.newDrawable()
            TextViewCompat.setTextAppearance(this, extensionListButtonTextAppearance)
            setTextColor(extensionListButtonTextColorStateList)
            isAllCaps = false //Doesn't work in Style resource
            textOn = extensionListButtonText
            textOff = extensionListButtonText
            isChecked = false
            isSelected = false

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                val outValue = TypedValue()
                context.theme.resolveAttribute(android.R.attr.selectableItemBackground, outValue, true)
                foreground = ResourcesCompat.getDrawable(context.resources, outValue.resourceId, context.theme)
            }
        }

        //Extension Popup
        extensionListButton.setOnClickListener {
            extensionListPopupWindow.showAtLocation((context as Activity).window.decorView, Gravity.CENTER, 0, 0)
        }

        extensionListPopupWindow.animationStyle = android.R.style.Animation_Dialog
        extensionListPopupWindow.setOnDismissListener {
            //extensionListButton.setOnCheckedChangeListener(null)
            updateExtensionListButtonState()
            //extensionListButton.setOnCheckedChangeListener(onCheckedChangeListener)
        }

        val flexLayoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        flexLayoutParams.flexBasisPercent = togglesFlexBasisPercent
        flexLayoutParams.order = 2 //Always at end
        super.addView(extensionListButton, -1, flexLayoutParams)
        extensionListButton.visibility = GONE

        //Attrs
        if (attrs != null) {
            val a = context.theme.obtainStyledAttributes(attrs, R.styleable.MultiQuantifiedToggleLayout, defStyleAttr, 0)

            try {
                extensionListButtonBackgroundSelector =
                    a.getDrawable(R.styleable.MultiQuantifiedToggleLayout_extensionListButtonBackgroundSelector)
                        ?: extensionListButtonBackgroundSelector

                extensionListButtonTextAppearance = a.getResourceId(
                    R.styleable.MultiQuantifiedToggleLayout_extensionListButtonTextAppearance,
                    extensionListButtonTextAppearance
                )

                extensionListButtonTextColorStateList =
                    a.getColorStateList(R.styleable.MultiQuantifiedToggleLayout_extensionListButtonTextColorSelector)
                        ?: extensionListButtonTextColorStateList


                togglesBackgroundSelector = a.getDrawable(R.styleable.MultiQuantifiedToggleLayout_togglesBackgroundSelector)
                    ?: togglesBackgroundSelector

                qtyBadgesBackground = a.getDrawable(R.styleable.MultiQuantifiedToggleLayout_qtyBadgesBackground)
                    ?: qtyBadgesBackground

                togglesTextAppearance = a.getResourceId(
                    R.styleable.MultiQuantifiedToggleLayout_togglesTextAppearance,
                    togglesTextAppearance
                )

                qtyBadgesTextAppearance = a.getResourceId(
                    R.styleable.MultiQuantifiedToggleLayout_qtyBadgesTextAppearance,
                    qtyBadgesTextAppearance
                )

                togglesTextColorStateList = a.getColorStateList(R.styleable.MultiQuantifiedToggleLayout_togglesTextColorSelector)
                    ?: togglesTextColorStateList

                qtyBadgesTextColor = a.getColor(
                    R.styleable.MultiQuantifiedToggleLayout_qtyBadgesTextColor,
                    qtyBadgesTextColor
                )

                togglesFlexBasisPercent = a.getFraction(
                    R.styleable.MultiQuantifiedToggleLayout_togglesFlexBasisPercent, 1, 1,
                    togglesFlexBasisPercent
                )


                isExtensionListEnabled =
                    a.getBoolean(R.styleable.MultiQuantifiedToggleLayout_extensionListEnabled, isExtensionListEnabled)

                extensionListButtonText = a.getString(R.styleable.MultiQuantifiedToggleLayout_extensionListButtonText)
                    ?: extensionListButtonText

                extensionListScreenTitle = a.getString(R.styleable.MultiQuantifiedToggleLayout_extensionListScreenTitle)
                    ?: extensionListScreenTitle

                mainListSize = a.getInt(R.styleable.MultiQuantifiedToggleLayout_mainListSize, mainListSize)

            } finally {
                a.recycle()
            }


        }

        togglesFlexBasisPercent = togglesFlexBasisPercent
    }

    private fun updateExtensionListButtonState() {
        extensionListButton.isSelected = getExtensionListCheckedTogglesCount() > 0
    }

    @JvmOverloads
    fun addQuantifiedToggle(
        stringId: String,
        textOn: String,
        textOff: String,
        onCheckedChangeListener: CompoundButton.OnCheckedChangeListener? = null,
        onQtyChangeListener: QuantifiedToggleButton.OnQtyChangeListener? = null,
        qtyEditPopupTitle: String = textOn,
        initialQty: Int = 0,
        minQty: Int = 0,
        maxQty: Int = 100,
        qtyStepSize: Int = 1,
        isCheckedByDefault: Boolean = false,
        childIndex: Int = -1
    ) {
        addToggle(
            stringId, textOn, textOff, true, onCheckedChangeListener, onQtyChangeListener,
            qtyEditPopupTitle, initialQty, minQty, maxQty, qtyStepSize, isCheckedByDefault, childIndex
        )
    }

    @JvmOverloads
    fun addSimpleToggle(
        stringId: String,
        textOn: String,
        textOff: String,
        onCheckedChangeListener: CompoundButton.OnCheckedChangeListener? = null,
        isCheckedByDefault: Boolean = false,
        childIndex: Int = -1
    ) {
        addToggle(
            stringId, textOn, textOff, false, onCheckedChangeListener,
            isCheckedByDefault = isCheckedByDefault, childIndex = childIndex
        )

    }

    private fun addToggle(
        stringId: String,
        textOn: String,
        textOff: String,
        isQuantified: Boolean,
        onCheckedChangeListener: CompoundButton.OnCheckedChangeListener? = null,
        onOnQtyChangeListener: QuantifiedToggleButton.OnQtyChangeListener? = null,
        qtyEditPopupTitle: String = textOn,
        initialQty: Int = 0,
        minQty: Int = 0,
        maxQty: Int = 100,
        qtyStepSize: Int = 1,
        isCheckedByDefault: Boolean = false,
        childIndex: Int = -1
    ) {

        if (getToggleViewIdByStringId(stringId) != null)
            throw IllegalArgumentException("Toggle with stringId \"$stringId\" already exists !")

        val newToggle = QuantifiedToggleButton(mContext)

        newToggle.apply {
            this.stringId = stringId
            this.isQuantified = isQuantified
            this.qtyEditPopupTitle = qtyEditPopupTitle
            this.minQty = minQty
            this.maxQty = maxQty
            this.qtyStepSize = qtyStepSize
            qty = initialQty
            this.onQtyChangeListener = onOnQtyChangeListener
            toggleButton.apply {
                this.background = togglesBackgroundSelector?.constantState?.newDrawable()
                this.textOn = textOn
                this.textOff = textOff
                TextViewCompat.setTextAppearance(this, togglesTextAppearance)
                setTextColor(togglesTextColorStateList)
                isChecked = isCheckedByDefault
                setOnCheckedChangeListener(onCheckedChangeListener)
            }
            qtyBadge.apply {
                this.background = qtyBadgesBackground
                TextViewCompat.setTextAppearance(this, qtyBadgesTextAppearance)
                setTextColor(qtyBadgesTextColor)
            }
        }

        val flexLayoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        flexLayoutParams.flexBasisPercent = togglesFlexBasisPercent
        val newViewId = generateViewId()
        newToggle.id = newViewId
        if (isExtensionListEnabled && (togglesCount + 1 > mainListSize)) {
            extensionListButton.visibility = VISIBLE
            extensionFlexBoxLayout.addView(newToggle, childIndex, flexLayoutParams)
            updateExtensionListButtonState()
        } else super.addView(newToggle, childIndex, flexLayoutParams)
        stringIdsMap[stringId] = newViewId

    }

    @JvmOverloads
    fun addToggle(
        stringId: String,
        quantifiedToggleButton: QuantifiedToggleButton,
        flexLayoutParams: LayoutParams,
        childIndex: Int = -1
    ) {
        val viewId: Int
        if (quantifiedToggleButton.id == NO_ID) {
            viewId = generateViewId()
            quantifiedToggleButton.id = viewId
        } else {
            viewId = quantifiedToggleButton.id
        }

        if (isExtensionListEnabled && (togglesCount + 1 > mainListSize)) {
            extensionListButton.visibility = VISIBLE
            extensionFlexBoxLayout.addView(quantifiedToggleButton, childIndex, flexLayoutParams)
            updateExtensionListButtonState()
        } else super.addView(quantifiedToggleButton, childIndex, flexLayoutParams)
        stringIdsMap[stringId] = viewId

    }


    fun removeToggle(stringId: String): Boolean {
        if (stringIdsMap.containsKey(stringId)) {

            val concernedToggle = getToggle(stringId)!!

            if (this.contains(concernedToggle)) {
                super.removeView(findViewById(stringIdsMap[stringId]!!))

                if (isExtensionListEnabled && extensionFlexBoxLayout.size > 0) {
                    val toggleToExport = extensionFlexBoxLayout[0] as QuantifiedToggleButton
                    val flexLayoutParams = toggleToExport.layoutParams as LayoutParams
                    extensionFlexBoxLayout.removeView(toggleToExport)
                    super.addView(toggleToExport, -1, flexLayoutParams)
                }

            } else { //In extensionFlexbox
                extensionFlexBoxLayout.removeView(concernedToggle)
            }
            stringIdsMap.remove(stringId)

            updateExtensionListButtonState()
            if (isExtensionListEnabled && extensionFlexBoxLayout.size == 0) extensionListButton.visibility = GONE

            return true
        }

        return false
    }


    fun getToggle(stringId: String): QuantifiedToggleButton? {
        return stringIdsMap[stringId]?.let { findViewById(it) ?: extensionFlexBoxLayout.findViewById(it) }
    }

    fun getToggleViewIdByStringId(stringId: String?): Int? {
        return stringIdsMap[stringId]
    }

    fun getStringId(toggleView: QuantifiedToggleButton): String? {
        forEach {
            if (it is QuantifiedToggleButton) {
                for (key in stringIdsMap.keys) {
                    if (stringIdsMap[key] == toggleView.id)
                        return key
                }
            }
        }
        //Look also in extension List
        extensionFlexBoxLayout.forEach {
            if (it is QuantifiedToggleButton) {
                for (key in stringIdsMap.keys) {
                    if (stringIdsMap[key] == toggleView.id)
                        return key
                }
            }
        }

        return null
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        //Blocking adding view from any other method other than #addToggle()

    }

    override fun addViewInLayout(
        child: View?,
        index: Int,
        params: ViewGroup.LayoutParams?,
        preventRequestLayout: Boolean
    ): Boolean {
        //Blocking adding view from any other method other than #addToggle()
        return false
    }

    override fun removeView(view: View?) {
        //Blocking removing view from any other method other than #removeToggle()
    }

    override fun removeViewInLayout(view: View?) {
        //Blocking removing view from any other method other than #removeToggle()
    }

    override fun removeViewsInLayout(start: Int, count: Int) {
        //Blocking removing view from any other method other than #removeToggle()
    }

    override fun removeViewAt(index: Int) {
        //Blocking removing view from any other method other than #removeToggle()
    }

    override fun removeViews(start: Int, count: Int) {
        //Blocking removing view from any other method other than #removeToggle()
    }

    override fun removeAllViews() {
        //Blocking removing view from any other method other than #removeToggle()
    }

    override fun removeAllViewsInLayout() {
        //Blocking removing view from any other method other than #removeToggle()
    }


    override fun onViewRemoved(child: View?) {
        super.onViewRemoved(child)
        //Don't allow to remove extensionListButton
        if (child == extensionListButton) {
            val flexLayoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            flexLayoutParams.order = super.getChildCount()
            flexLayoutParams.flexBasisPercent = togglesFlexBasisPercent
            super.addView(extensionListButton, -1, flexLayoutParams)
        }
    }

    fun getExtensionListCheckedTogglesCount(): Int {
        var checkedCount = 0
        extensionFlexBoxLayout.forEach {
            if (it is QuantifiedToggleButton) {
                if (it.toggleButton.isChecked) ++checkedCount
            }
        }
        return checkedCount
    }


    //TODO Implement adding QuantifiedToggle in layout
    class QuantifiedToggleButton constructor(context: Context) : FrameLayout(context) {

        val toggleButton = ToggleButton(context)
        val qtyBadge = Button(context)

        lateinit var stringId : String

        private val qtyPopupLayout = LayoutInflater.from(context).inflate(
            R.layout
                .multi_quantified_toggle_layout_qty_popup_layout, null
        )

        private val qtyPopupWindow =
            PopupWindow(
                qtyPopupLayout,
                dpToPx(context.resources, 152),
                dpToPx(context.resources, 112),
                true
            )


        var isQuantified = false
            set(value) {
                qtyBadge.isVisible = value
                if (value)
                    toggleButton.setOnLongClickListener {
                        qtyPopupWindow.showAtLocation((context as Activity).window.decorView, Gravity.CENTER, 0, 0)
                        false
                    }
                else setOnLongClickListener(null)
                field = value
            }

        var qtyEditPopupTitle = ""
            set(value) {
                qtyPopupLayout.findViewById<TextView>(R.id.title).text = value
                field = value
            }

        var qtyStepSize = 1
            set(value) {
                qtyPopupLayout.findViewById<NumberPicker>(R.id.number_picker).stepSize = value.toDouble()
                field = value
            }

        var minQty = 0
            set(value) {
                qtyPopupLayout.findViewById<NumberPicker>(R.id.number_picker).minValue = value.toDouble()
                field = value
            }

        var maxQty = 100
            set(value) {
                qtyPopupLayout.findViewById<NumberPicker>(R.id.number_picker).maxValue = value.toDouble()
                field = value
            }

        var qty = 0 //Automatically unchecked
            set(value) {
                if (value != qty) {
                    if (value <= 1) {
                        toggleButton.isChecked = value == 1
                        qtyBadge.text = "+"
                    } else {
                        toggleButton.isChecked = true
                        qtyBadge.text = value.toString()
                    }
                    qtyPopupLayout.findViewById<NumberPicker>(R.id.number_picker).setProgress(BigDecimal.valueOf(value.toLong()))
                    field = value
                    onQtyChangeListener?.onChange(this, field, toggleButton.isChecked)
                }
            }

        var onQtyChangeListener: OnQtyChangeListener? = null

        interface OnQtyChangeListener {
            fun onChange(quantifiedToggleButton: QuantifiedToggleButton, qty: Int, isChecked: Boolean)
        }

        init {
            //ToggleButton Defaults
            toggleButton.apply {
                TextViewCompat.setTextAppearance(this, R.style.TextAppearance_AppCompat_Body2)
                setTextColor(
                    ResourcesCompat.getColorStateList(
                        context.resources,
                        R.color.text_radio_toggle_color_selector,
                        context.theme
                    )
                )
                isAllCaps = false //Doesn't work in Style resource
                z = 0f
                stateListAnimator = null // Default state list animator causes changes in views' elevation

            }
            addView(toggleButton, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

            //Quantity badge defaults
            qtyBadge.apply {
                setPadding(
                    dpToPx(context.resources, 3),
                    dpToPx(context.resources, 2),
                    dpToPx(context.resources, 3),
                    dpToPx(context.resources, 1)
                )
                minHeight = 0
                minimumHeight = 0
                minWidth = dpToPx(context.resources, 20)
                minimumWidth = dpToPx(context.resources, 20)
                z = dpToPx(context.resources, 5).toFloat()

                stateListAnimator = null // Default state list animator causes changes in views' elevation


            }
            TextViewCompat.setTextAppearance(qtyBadge, R.style.QuantifiedToggleButtonQtyBadgeTextAppearance)
            qtyBadge.isAllCaps = false //Doesn't work if specified in Style resource only
            val qtyBadgeLayoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            qtyBadgeLayoutParams.gravity = Gravity.TOP or Gravity.END
            addView(qtyBadge, qtyBadgeLayoutParams)

            //Initialization
            qty = 0
            isQuantified = false

            //qty Configs
            qtyPopupLayout.findViewById<NumberPicker>(R.id.number_picker)
                .doOnProgressChanged { numberPicker, progress, formUser ->
                    qty = progress.toInt()
                }

            qtyPopupWindow.animationStyle = android.R.style.Animation_Dialog
            if (isQuantified) {
                toggleButton.setOnLongClickListener {
                    qtyPopupWindow.showAtLocation((context as Activity).window.decorView, Gravity.CENTER, 0, 0)
                    false
                }
            }

        }
    }

}

package com.ouedyan.immofind

import com.ouedyan.immofind.model.Property
import java.util.*

object PropertyBank {
    private val calendar = Calendar.getInstance()
    var mProperties = ArrayList<Property>()
    val allProperties: ArrayList<Property>
        get() = ArrayList(mProperties)
    val rentedProperties: ArrayList<Property>
        get() {
            val rented = ArrayList<Property>()
            for (property in mProperties) if (property.status == Property.PropertyStatus.FOR_RENT) rented.add(property)
            return rented
        }
    val forSaleProperties: ArrayList<Property>
        get() {
            val forSale = ArrayList<Property>()
            for (property in mProperties) if (property.status == Property.PropertyStatus.FOR_SALE) forSale.add(property)
            return forSale
        }

}

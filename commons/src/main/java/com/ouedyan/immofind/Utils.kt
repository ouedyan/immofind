package com.ouedyan.immofind

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.util.TypedValue
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.annotation.ColorInt
import com.ouedyan.immofind.ui.CustomTabSpan


object Utils {
    fun setTosAndPpTextView(
        context: Context,
        textView: TextView,
        wholeText: String,
        tosText: String,
        ppText: String,
        tosUrl: String,
        ppUrl: String,
        @ColorInt textColor: Int
    ) {
        val builder = SpannableStringBuilder(wholeText)
        setLinkSpan(context, builder, tosText, tosUrl, textColor)
        setLinkSpan(context, builder, ppText, ppUrl, textColor)
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.text = builder
    }
    
    fun setLinkSpan(context: Context, builder: SpannableStringBuilder, target: String, url: String, @ColorInt spanColor: Int) {
        val colorSpan = ForegroundColorSpan(spanColor)
        val tabSpan = CustomTabSpan(context, url)
        val start = builder.toString().indexOf(target)
        val end = start + target.length
        builder.setSpan(colorSpan, start, end, 0)
        builder.setSpan(tabSpan, start, end, 0)
    }
    
    /**
     * Convert the given dps value in Px
     *
     * @param res Resources from which to get the display metrics
     * @param dps Value in dps to convert in Px
     */
    fun dpToPx(res: Resources, dps: Int): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dps.toFloat(), res.displayMetrics).toInt()
    }
    
    /**
     * @return view's id name specified in xml or "no-id" if there is no id
     */
    fun getStringIdFromXmlId(view: View): String? {
        return if (view.id == View.NO_ID) "no-id" else view.resources.getResourceEntryName(view.id)
    }
    
    fun setActivityStatusBarColor(activity: Activity, @ColorInt color: Int) {
        val window: Window = activity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = color
    }
    
}
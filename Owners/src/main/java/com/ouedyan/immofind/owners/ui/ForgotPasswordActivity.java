package com.ouedyan.immofind.owners.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ouedyan.immofind.owners.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }
}
package com.ouedyan.immofind.owners.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.chinalwb.are.AREditText
import com.chivorn.smartmaterialspinner.SmartMaterialSpinner
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.android.material.slider.Slider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.ouedyan.imagechooser.ImageChooser
import com.ouedyan.immofind.model.City
import com.ouedyan.immofind.Location
import com.ouedyan.immofind.owners.R
import com.ouedyan.immofind.Utils.dpToPx
import com.ouedyan.immofind.owners.firestoreapi.PropertiesHelper
import com.ouedyan.immofind.model.Property
import com.ouedyan.immofind.model.Property.AreaUnit
import com.ouedyan.immofind.model.Property.TypeOfProperty
import com.ouedyan.immofind.model.Tag
import com.ouedyan.immofind.ui.MultiQuantifiedToggleLayout
import it.sephiroth.android.library.numberpicker.NumberPicker
import it.sephiroth.android.library.numberpicker.doOnProgressChanged
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class NewPostActivity : AppCompatActivity() {
    private val richTextRequestCode = 1
    private var onImageUploadCallbacks: OnUploadImageCallbacks? = null
    
    //TODO Review SmartMaterialSpinners often not responding on click on UI when nothing chosen...
    private lateinit var citySpinner: SmartMaterialSpinner<String>
    private lateinit var locationInCityAutoComplete: AutoCompleteTextView
    private lateinit var radioGroupStatus: RadioGroup
    private lateinit var typeOfPropertySpinner: SmartMaterialSpinner<String>
    private lateinit var mImageChooser: ImageChooser
    private lateinit var mPriceNumberPicker: NumberPicker
    private lateinit var editShortDescription: EditText
    private lateinit var richEditDetailedDescription: AREditText
    
    //Habitation
    private lateinit var numberPickerSurfaceHabitation: NumberPicker
    private lateinit var numberPickerLandSurfaceHabitation: NumberPicker
    private lateinit var numberPickerRoomsHabitation: NumberPicker
    private lateinit var numberPickerBedroomsHabitation: NumberPicker
    private lateinit var habitationSpecificitiesMultiToggle: MultiQuantifiedToggleLayout
    
    //Land or Farm
    private lateinit var numberPickerLandSurfaceLandOrFarm: NumberPicker
    private var landOrFarmLandSurfaceUnit: AreaUnit? = null
    private lateinit var landOrFarmSpecificitiesMultiToggle: MultiQuantifiedToggleLayout
    
    //Building
    private lateinit var numberPickerSurfaceBuilding: NumberPicker
    private lateinit var numberPickerRoomsBuilding: NumberPicker
    private lateinit var buildingSpecificitiesMultiToggle: MultiQuantifiedToggleLayout
    
    //Office rooms
    private lateinit var numberPickerOfficeRooms: NumberPicker
    
    
    private var selectedCity: City? = null
    private var typeOfProperty: TypeOfProperty? = null
    
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_post)
        
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        
        //Habitation
        numberPickerSurfaceHabitation = findViewById(R.id.number_picker_surface_habitation)
        numberPickerLandSurfaceHabitation = findViewById(R.id.number_picker_land_surface_habitation)
        numberPickerRoomsHabitation = findViewById(R.id.number_picker_rooms_habitation)
        numberPickerBedroomsHabitation = findViewById(R.id.number_picker_bedrooms_habitation)
        
        //Land or Farm
        numberPickerLandSurfaceLandOrFarm = findViewById(R.id.number_picker_land_surface_land_or_farm)
        
        //Building
        numberPickerSurfaceBuilding = findViewById(R.id.number_picker_surface_building)
        numberPickerRoomsBuilding = findViewById(R.id.number_picker_rooms_building)
        
        //Office rooms
        numberPickerOfficeRooms = findViewById(R.id.number_picker_office_rooms_office_rooms)
        
        //City spinner setup
        citySpinner = findViewById(R.id.spinner_city)
        setupCitySpinner()
        
        //Enabling location in city auto complete editText according to city is selected or not
        locationInCityAutoComplete = findViewById(R.id.auto_complete_location_in_city)
        setupLocationInCityEditText()
        
        //Min and max values of type of properties' characteristics setting in accordance to selected
        // status(for rent or sale)
        radioGroupStatus = findViewById(R.id.radio_rent_or_sale)
        radioGroupStatus.clearCheck()
        radioGroupStatus.check(R.id.radio_rent) // Check for rent by default
        linkSlidersAndNumberPickers()
        
        //Type of property spinner setup
        typeOfPropertySpinner = findViewById(R.id.spinner_type_of_property)
        setupTypeOfPropertySpinner()
        
        mImageChooser = findViewById(R.id.imagechooser)
        
        mPriceNumberPicker = findViewById(R.id.number_picker_price)
        
        //Habitation block specificities setup
        habitationSpecificitiesMultiToggle = findViewById(R.id.habitation_specificities)
        habitationSpecificitiesMultiToggle.apply {
            addSimpleToggle(
                Property.HabitationSpecificity.FURNISHED.name,
                resources.getString(R.string.furnished),
                resources.getString(R.string.furnished)
            )
            addSimpleToggle(
                Property.HabitationSpecificity.ACCESS_TO_ELECTRICITY.name,
                resources.getString(R.string.access_to_electricity),
                resources.getString(R.string.access_to_electricity)
            )
            addSimpleToggle(
                Property.HabitationSpecificity.ACCESS_TO_WATER.name,
                resources.getString(R.string.access_to_water),
                resources.getString(R.string.access_to_water)
            )
            addSimpleToggle(
                Property.HabitationSpecificity.FENCED.name,
                resources.getString(R.string.fenced),
                resources.getString(R.string.fenced)
            )
            addSimpleToggle(
                Property.HabitationSpecificity.AIR_CONDITIONED.name,
                resources.getString(R.string.air_conditioned),
                resources.getString(R.string.air_conditioned)
            )
            addQuantifiedToggle(
                Property.HabitationSpecificity.GARAGES.name,
                resources.getString(R.string.garages),
                resources.getString(R.string.garages)
            )
            addQuantifiedToggle(
                Property.HabitationSpecificity.SWIMMING_POOLS.name,
                resources.getString(R.string.swimming_pools),
                resources.getString(R.string.swimming_pools)
            )
        }
        
        //Land/Farm block specificities setup
        landOrFarmSpecificitiesMultiToggle = findViewById(R.id.land_or_farm_specificities)
        landOrFarmSpecificitiesMultiToggle.apply {
            addSimpleToggle(
                Property.LandOrFarmSpecificity.ACCESS_TO_ELECTRICITY.name,
                resources.getString(R.string.access_to_electricity),
                resources.getString(R.string.access_to_electricity)
            )
            addSimpleToggle(
                Property.LandOrFarmSpecificity.ACCESS_TO_WATER.name,
                resources.getString(R.string.access_to_water),
                resources.getString(R.string.access_to_water)
            )
            addSimpleToggle(
                Property.LandOrFarmSpecificity.FENCED.name,
                resources.getString(R.string.fenced),
                resources.getString(R.string.fenced)
            )
            
        }
        
        //Building block specificities setup
        buildingSpecificitiesMultiToggle = findViewById(R.id.building_specificities)
        buildingSpecificitiesMultiToggle.apply {
            addSimpleToggle(
                Property.BuildingSpecificity.FENCED.name,
                resources.getString(R.string.fenced),
                resources.getString(R.string.fenced)
            )
            addSimpleToggle(
                Property.BuildingSpecificity.PARKING.name,
                resources.getString(R.string.parking),
                resources.getString(R.string.parking)
            )
            addQuantifiedToggle(
                Property.BuildingSpecificity.GARAGES.name,
                resources.getString(R.string.garages),
                resources.getString(R.string.garages)
            )
            
        }
        
        
        //Land or farm land surface surface unit chooser spinner setup an linking with the numberPicker
        //Surface unit chooser spinner setup
        val landOrFarmLandSurfaceUnitSpinner =
            findViewById<SmartMaterialSpinner<String>>(R.id.spinner_land_surface_unit_land_or_farm)
        setupSurfaceUnitSpinner(landOrFarmLandSurfaceUnitSpinner)
        
        //Linking with the numberPicker
        setupLandOrFarmLandSurfaceUnitSpinnerWithNumberPicker()
        
        //Adding clearing focus of the editor to the default #onEditorAction() behavior.
        val customOnEditorActionListener = OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                v.clearFocus()
                hideKeyboard()
            }
            false
        }
        editShortDescription = findViewById(R.id.edit_short_description)
        editShortDescription.setOnEditorActionListener(customOnEditorActionListener)
        locationInCityAutoComplete.setOnEditorActionListener(customOnEditorActionListener)
        
        //Wrapping lines manually instead of automatically(putting textMultiline flag on inputType) on simple
        // editText, because textMultiline displays Enter at imeAction DONE place .
        // Setting it from xml doesn't work
        editShortDescription.setHorizontallyScrolling(false)
        editShortDescription.maxLines = 4
        
        
        //Detailed description rich text editor styling
        richEditDetailedDescription = findViewById(R.id.rich_edit_detailed_description)
        richEditDetailedDescription.textSize = 14f
        richEditDetailedDescription.background = ContextCompat.getDrawable(this, android.R.color.transparent)
        richEditDetailedDescription.isFocusable = false
        richEditDetailedDescription.isFocusableInTouchMode = false
        richEditDetailedDescription.setPadding(
            dpToPx(resources, 20),
            dpToPx(resources, 7),
            dpToPx(resources, 20),
            dpToPx(resources, 7)
        )
        
        findViewById<Button>(R.id.button_post).setOnClickListener { post() }
    }
    
    private fun setupLandOrFarmLandSurfaceUnitSpinnerWithNumberPicker() {
        val landOrFarmLandSurfaceUnitSpinner = findViewById<SmartMaterialSpinner<String>>(
            R.id.spinner_land_surface_unit_land_or_farm
        )
        val areaUnitList = AreaUnit.values()
        landOrFarmLandSurfaceUnitSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                val selected = areaUnitList[position]
                if (landOrFarmLandSurfaceUnit != selected) {
                    landOrFarmLandSurfaceUnit = selected
                    val landOrFarmLandSurfaceSlider = findViewById<Slider>(R.id.slider_land_surface_land_or_farm)
                    when (selected) {
                        AreaUnit.SQUARE_METERS -> {
                            numberPickerLandSurfaceLandOrFarm.minValue =
                                resources.getInteger(R.integer.land_or_farm_min_land_surface_m2).toDouble()
                            numberPickerLandSurfaceLandOrFarm.maxValue =
                                resources.getInteger(R.integer.land_or_farm_max_land_surface_m2).toDouble()
                            numberPickerLandSurfaceLandOrFarm.setProgress(
                                resources.getInteger(R.integer.land_or_farm_default_land_surface_m2).toBigDecimal()
                            )
                            
                            landOrFarmLandSurfaceSlider.valueFrom =
                                resources.getInteger(R.integer.land_or_farm_min_land_surface_m2).toFloat()
                            landOrFarmLandSurfaceSlider.valueTo =
                                resources.getInteger(R.integer.land_or_farm_max_land_surface_m2).toFloat()
                            //Don't forget when min and max set programmatically or error
                            landOrFarmLandSurfaceSlider.value =
                                resources.getInteger(R.integer.land_or_farm_default_land_surface_m2).toFloat()
                        }
                        AreaUnit.HECTARES -> {
                            numberPickerLandSurfaceLandOrFarm.minValue =
                                resources.getInteger(R.integer.land_or_farm_min_land_surface_ha).toDouble()
                            numberPickerLandSurfaceLandOrFarm.maxValue =
                                resources.getInteger(R.integer.land_or_farm_max_land_surface_ha).toDouble()
                            numberPickerLandSurfaceLandOrFarm.setProgress(
                                resources.getInteger(R.integer.land_or_farm_default_land_surface_ha).toBigDecimal()
                            )
                            
                            landOrFarmLandSurfaceSlider.valueFrom =
                                resources.getInteger(R.integer.land_or_farm_min_land_surface_ha).toFloat()
                            landOrFarmLandSurfaceSlider.valueTo =
                                resources.getInteger(R.integer.land_or_farm_max_land_surface_ha).toFloat()
                            //Don't forget when min and max set programmatically or error
                            landOrFarmLandSurfaceSlider.value =
                                resources.getInteger(R.integer.land_or_farm_default_land_surface_ha).toFloat()
                        }
                    }
                }
            }
            
            override fun onNothingSelected(parent: AdapterView<*>?) {
                landOrFarmLandSurfaceUnit = null
                //TODO Impossible to not choose
            }
        }
        landOrFarmLandSurfaceUnitSpinner.setSelection(1) //Default unit selection
    }
    
    private fun setupSurfaceUnitSpinner(landOrFarmLandSurfaceUnitSpinner: SmartMaterialSpinner<String>) {
        val areaUnitList = AreaUnit.values()
        val areaUnitStringList = ArrayList<String>()
        areaUnitList.forEach { areaUnitStringList.add(getString(it.stringRes)) }
        landOrFarmLandSurfaceUnitSpinner.item = areaUnitStringList
    }
    
    private fun setupTypeOfPropertySpinner() {
        val typeOfPropertyList = TypeOfProperty.values()
        //Getting strings
        val typeOfPropertyStringList = ArrayList<String>()
        typeOfPropertyList.forEach { typeOfPropertyStringList.add(getString(it.stringRes)) }
        //Setting spinner
        typeOfPropertySpinner.item = typeOfPropertyStringList
        typeOfPropertySpinner.selectedItem
        typeOfPropertySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                val selected = typeOfPropertyList[position]
                if (typeOfProperty != selected) {
                    typeOfProperty = selected
                    onTypeOfPropertySelected(selected)
                }
            }
            
            override fun onNothingSelected(parent: AdapterView<*>?) {
                typeOfProperty = null
                onTypeOfPropertySelected(null)
            }
        }
    }
    
    private fun setupLocationInCityEditText() {
        val cities = ArrayList<City>()
        Location.getCountry(
            Locale(
                Location.currentLocale.substringBefore('_'),
                Location.currentLocale.substringAfter('_')
            ).country
        )?.let { cities.addAll(it.cities) }
        locationInCityAutoComplete.inputType = EditorInfo.TYPE_NULL
        citySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                val selectedCity = cities[position]
                if (this@NewPostActivity.selectedCity != selectedCity)
                    this@NewPostActivity.selectedCity = selectedCity
                
                locationInCityAutoComplete.apply {
                    isEnabled = true
                    text = null
                    inputType = EditorInfo.TYPE_TEXT_VARIATION_SHORT_MESSAGE
                    inputType = EditorInfo.TYPE_TEXT_FLAG_CAP_WORDS
                    val sectors = ArrayList<String>()
                    cities[position].numberOfSectors?.let {
                        if (it > 1)
                            for (i in 1..it) sectors.add(getString(R.string.sector) + " $i")
                    }
                    val neighborhoods = ArrayList<String>()
                    cities[position].neighborhoods?.let { neighborhoods += it }
                    setAdapter(
                        ArrayAdapter(
                            this@NewPostActivity,
                            android.R.layout.simple_dropdown_item_1line,
                            (neighborhoods + sectors)
                        )
                    )
                    requestFocus()
                }
            }
            
            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedCity = null
                
                locationInCityAutoComplete.text = null
                locationInCityAutoComplete.inputType = EditorInfo.TYPE_NULL
                locationInCityAutoComplete.isEnabled = false
            }
        }
        
        //Clearing focus from location in city auto complete editText after a suggestion has been selected
        //(note : OnItemSelectedListener doesn't work)
        locationInCityAutoComplete.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            locationInCityAutoComplete.onEditorAction(EditorInfo.IME_ACTION_DONE)
        }
    }
    
    private fun setupCitySpinner() {
        val cities = ArrayList<City>()
        Location.getCountry(
            Locale(
                Location.currentLocale.substringBefore('_'),
                Location.currentLocale.substringAfter('_')
            ).country
        )?.let {
            cities.addAll(
                it
                    .cities
            )
        }
        val citiesNames = ArrayList<String>()
        for (city in cities) citiesNames.add(city.name)
        citySpinner.item = citiesNames
    }
    
    fun openRichTextEditor(view: View?) {
        val html = richEditDetailedDescription.html
        val intent = Intent(this, RichEditActivity::class.java)
        //TODO Retrieve values from resources for translation
        intent.putExtra("title", "Detailed description")
        intent.putExtra("hint", "Enter property's detailed description here...")
        intent.putExtra("html", html)
        startActivityForResult(intent, richTextRequestCode)
    }
    
    private fun linkSlidersAndNumberPickers() {
        //Habitation
        val sliderSurfaceHabitation = findViewById<Slider>(R.id.slider_surface_habitation)
        linkSliderAndNumberPicker(sliderSurfaceHabitation, numberPickerSurfaceHabitation)
        val sliderLandSurfaceHabitation = findViewById<Slider>(R.id.slider_land_surface_habitation)
        linkSliderAndNumberPicker(sliderLandSurfaceHabitation, numberPickerLandSurfaceHabitation)
        val sliderRoomsHabitation = findViewById<Slider>(R.id.slider_rooms_habitation)
        linkSliderAndNumberPicker(sliderRoomsHabitation, numberPickerRoomsHabitation)
        val sliderBedroomsHabitation = findViewById<Slider>(R.id.slider_bedrooms_habitation)
        linkSliderAndNumberPicker(sliderBedroomsHabitation, numberPickerBedroomsHabitation)
        
        //Land or Farm
        val sliderLandSurfaceLandOrFarm = findViewById<Slider>(R.id.slider_land_surface_land_or_farm)
        linkSliderAndNumberPicker(sliderLandSurfaceLandOrFarm, numberPickerLandSurfaceLandOrFarm)
        
        //Building
        val sliderSurfaceBuilding = findViewById<Slider>(R.id.slider_surface_building)
        linkSliderAndNumberPicker(sliderSurfaceBuilding, numberPickerSurfaceBuilding)
        val sliderRoomsBuilding = findViewById<Slider>(R.id.slider_rooms_building)
        linkSliderAndNumberPicker(sliderRoomsBuilding, numberPickerRoomsBuilding)
        
        //Office rooms
        val sliderOfficeRooms = findViewById<Slider>(R.id.slider_office_rooms_office_rooms)
        linkSliderAndNumberPicker(sliderOfficeRooms, numberPickerOfficeRooms)
    }
    
    private fun linkSliderAndNumberPicker(slider: Slider, numberPicker: NumberPicker) {
        numberPicker.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            slider.value = progress.toFloat()
        }
        slider.addOnChangeListener { thisSlider, value, fromUser ->
            numberPicker.setProgress(thisSlider.value.toBigDecimal().stripTrailingZeros())
        }
    }
    
    private fun linkSliderAndNumberPicker(slider: Slider, numberPicker: NumberPicker, minValue: Int, maxValue: Int) {
        //TO review computing default value algorithm
        numberPicker.minValue = minValue.toDouble()
        numberPicker.maxValue = maxValue.toDouble()
        numberPicker.setProgress((minValue + maxValue shr 1).toBigDecimal())
        //TO see result
        slider.valueFrom = minValue.toFloat()
        slider.valueTo = maxValue.toFloat()
        slider.value = (minValue + maxValue shr 1).toFloat()
        numberPicker.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            slider.value = progress.toFloat()
        }
        slider.addOnChangeListener { thisSlider, value, fromUser ->
            numberPicker.setProgress(thisSlider.value.toBigDecimal().stripTrailingZeros())
        }
    }
    
    private fun setupLinkedSliderAndNumberPickerDefaultValues(
        slider: Slider, numberPicker: NumberPicker,
        minValue: Int,
        defaultValue: Int,
        maxValue: Int
    ) {
        numberPicker.maxValue = maxValue.toDouble()
        numberPicker.minValue = minValue.toDouble() //A bit shit : mandatory to set maxValue value
        // before minValue value in order for minValue to not be greater than maxValue original value
        numberPicker.setProgress(defaultValue.toBigDecimal())
        
        slider.valueFrom = minValue.toFloat()
        slider.valueTo = maxValue.toFloat()
        slider.value = defaultValue.toFloat()
    }
    
    private fun onTypeOfPropertySelected(typeOfProperty: TypeOfProperty?) {
        val allBlocksIds = listOf(
            R.id.habitation_block,
            R.id.land_or_farm_block,
            R.id.building_block,
            R.id.office_rooms_block
        )
        for (blockId in allBlocksIds) findViewById<View>(blockId).isVisible = false
        
        val selectedBlockId: Int? = when (typeOfProperty) {
            TypeOfProperty.HABITATION -> R.id.habitation_block
            
            TypeOfProperty.LAND_OR_LAND_LOT,
            TypeOfProperty.FARM -> R.id.land_or_farm_block
            
            TypeOfProperty.BUILDING -> R.id.building_block
            
            TypeOfProperty.OFFICE_ROOMS -> R.id.office_rooms_block
            
            TypeOfProperty.COMMERCIAL_PLACE,
            TypeOfProperty.STORE,
            TypeOfProperty.FACTORY -> null
            
            else -> null
        }
        selectedBlockId?.let { findViewById<View>(it).isVisible = true }
    }
    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == richTextRequestCode) {
            if (resultCode == RESULT_OK && data != null) {
                richEditDetailedDescription.text = null //do not append
                richEditDetailedDescription.fromHtml(data.getStringExtra("html"))
            }
        } else if (requestCode == ImageChooser.REQUEST_CODE) {
            mImageChooser.handleImagePickerResult(data, resultCode)
        }
    }
    
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    
    private fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window.decorView.windowToken, 0)
    }
    
    
    private fun post() {
        if (!validateFields()) return
        val newProperty = Property()
        newProperty.apply {
            ownerUid = FirebaseAuth.getInstance().currentUser!!.uid
            status = if (radioGroupStatus.checkedRadioButtonId == R.id.radio_rent)
                Property.PropertyStatus.FOR_RENT else Property.PropertyStatus.FOR_SALE
            cityCode = selectedCity!!.code
            locationInCity = locationInCityAutoComplete.text.toString()
            shortDescription = editShortDescription.text.toString()
            htmlDetailedDescription = richEditDetailedDescription.html
            //propertyPicturesUrls = mImageChooser.getImagesPaths()!!
            
            locale = Location.currentLocale
            if (status == Property.PropertyStatus.FOR_RENT) {
                rentIntervalUnit = Property.RentIntervalUnit.MONTH
                rentInterval = 1
                rentPrice = mPriceNumberPicker.getProgress().toDouble()
            } else //for sale
            {
                salePrice = mPriceNumberPicker.getProgress().toDouble()
            }
            
            when (typeOfProperty!!) {
                TypeOfProperty.HABITATION -> {
                    val habitationSpecificities = getHabitationSpecificities()
                    /*typeOfPropertyData = Property.TypeOfPropertyData.Habitation(
                        object : Property.HabitationDetails() {
                            override val surface = numberPickerSurfaceHabitation.getProgress().toDouble()
                            override val landSurface = numberPickerLandSurfaceHabitation.getProgress().toDouble()
                            override val numberOfRooms = numberPickerRoomsHabitation.getProgress().toInt()
                            override val numberOfBedrooms = numberPickerBedroomsHabitation.getProgress().toInt()
                            override val specificity = habitationSpecificities

                        }
                    )*/
                    tags.addAll(
                        listOf(
                            Tag(resources.getResourceEntryName(R.string.habitation), type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG),
                            Tag(
                                resources.getResourceEntryName(R.string.rooms),
                                numberPickerRoomsHabitation.getProgress().toInt()
                            ),
                            Tag(
                                resources.getResourceEntryName(R.string.bedrooms), numberPickerBedroomsHabitation.getProgress()
                                    .toInt()
                            )
                        )
                    )
                    habitationSpecificities.forEach {
                        tags.add(Tag(resources.getResourceEntryName(it.habitationSpecificity.stringRes), it.qty))
                    }
                    areaUnit = AreaUnit.SQUARE_METERS
                    area = numberPickerSurfaceHabitation.getProgress().toDouble()
                }
                TypeOfProperty.LAND_OR_LAND_LOT -> {
                    val landOrFarmSpecificities = getLandOrFarmSpecificities()
                    /*typeOfPropertyData = Property.TypeOfPropertyData.LandOrLandLot(
                        object : Property.LandOrFarmDetails() {
                            override val landSurface = numberPickerLandSurfaceLandOrFarm.getProgress().toDouble()
                            override val specificity = landOrFarmSpecificities

                        }
                    )*/
                    tags.add(
                        Tag(
                            resources.getResourceEntryName(R.string.land_or_land_lot),
                            type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG
                        )
                    )
                    landOrFarmSpecificities.forEach {
                        tags.add(Tag(resources.getResourceEntryName(it.landOrFarmSpecificity.stringRes), it.qty))
                    }
                    areaUnit = landOrFarmLandSurfaceUnit
                    area = numberPickerLandSurfaceLandOrFarm.getProgress().toDouble()
                }
                TypeOfProperty.FARM -> {
                    val landOrFarmSpecificities = getLandOrFarmSpecificities()
                    /*typeOfPropertyData = Property.TypeOfPropertyData.Farm(
                        object : Property.LandOrFarmDetails() {
                            override val landSurface = numberPickerLandSurfaceLandOrFarm.getProgress().toDouble()
                            override val specificity = landOrFarmSpecificities

                        }
                    )*/
                    tags.add(Tag(resources.getResourceEntryName(R.string.farm), type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG))
                    landOrFarmSpecificities.forEach {
                        tags.add(Tag(resources.getResourceEntryName(it.landOrFarmSpecificity.stringRes), it.qty))
                    }
                    areaUnit = landOrFarmLandSurfaceUnit
                    area = numberPickerLandSurfaceLandOrFarm.getProgress().toDouble()
                }
                TypeOfProperty.BUILDING -> {
                    val buildingSpecificities = getBuildingSpecificities()
                    /*typeOfPropertyData = Property.TypeOfPropertyData.Building(
                        object : Property.BuildingDetails() {
                            override val surface = numberPickerSurfaceBuilding.getProgress().toDouble()
                            override val numberOfRooms = numberPickerRoomsBuilding.getProgress().toInt()
                            override val specificity = buildingSpecificities

                        }
                    )*/
                    tags.add(Tag(resources.getResourceEntryName(R.string.building), type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG))
                    tags.add(Tag(resources.getResourceEntryName(R.string.rooms), numberPickerRoomsBuilding.getProgress().toInt()))
                    buildingSpecificities.forEach {
                        tags.add(Tag(resources.getResourceEntryName(it.buildingSpecificity.stringRes), it.qty))
                    }
                    areaUnit = AreaUnit.SQUARE_METERS
                    area = numberPickerSurfaceBuilding.getProgress().toDouble()
                }
                TypeOfProperty.OFFICE_ROOMS -> {
                    /*typeOfPropertyData = Property.TypeOfPropertyData.OfficeRooms(
                        object : Property.OfficeRoomsDetails() {
                            override val numberOfOfficeRooms = numberPickerOfficeRooms.getProgress().toInt()

                        }
                    )*/
                    tags.add(Tag(resources.getResourceEntryName(R.string.office_room), type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG))
                    tags.add(
                        Tag(
                            resources.getResourceEntryName(R.string.office_rooms),
                            numberPickerRoomsBuilding.getProgress().toInt()
                        )
                    )
                    areaUnit = null
                    area = null
                }
                
                TypeOfProperty.COMMERCIAL_PLACE -> {
                    //typeOfPropertyData = Property.TypeOfPropertyData.CommercialPlace()
                    tags.add(
                        Tag(
                            resources.getResourceEntryName(R.string.commercial_place),
                            type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG
                        )
                    )
                    areaUnit = null
                    area = null
                }
                TypeOfProperty.STORE -> {
                    //typeOfPropertyData = Property.TypeOfPropertyData.Store()
                    tags.add(Tag(resources.getResourceEntryName(R.string.store), type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG))
                    areaUnit = null
                    area = null
                }
                TypeOfProperty.FACTORY -> {
                    //typeOfPropertyData = Property.TypeOfPropertyData.Factory()
                    tags.add(Tag(resources.getResourceEntryName(R.string.factory), type = Tag.TAG_TYPE.PROPERTY_TYPE_TAG))
                    areaUnit = null
                    area = null
                }
            }
            
            state = Property.State.AVAILABLE //Default state
            
        }
        
        //TODO Implement image auto deletion of images with Cloud Functions
        val db = FirebaseFirestore.getInstance()
        val newPropertyRef = PropertiesHelper.propertiesCollection.document()
        val propertyImagesUrls = mImageChooser.getImagesPaths()!!
        val propertyPicturesUrlsIterator = propertyImagesUrls.listIterator()
        
        
        uploadPropertyImages(newPropertyRef.id, propertyPicturesUrlsIterator,
            object : OnUploadImageCallbacks {
                override fun onSuccess(index: Int) {
                    Toast.makeText(this@NewPostActivity, "Image $index loaded successfully.", Toast.LENGTH_SHORT).show()
                }
                
                override fun onFailed(index: Int, e: Exception?) {
                    Toast.makeText(this@NewPostActivity, "Image $index loading failed.", Toast.LENGTH_SHORT).show()
                    e?.printStackTrace()
                }
                
            })
            ?.addOnSuccessListener {
                
                //Save property details in the database
                newProperty.apply {
                
                }
                newPropertyRef.set(newProperty)
                    .addOnSuccessListener {
                        //TODO Tests purposes
                        /*newProperty.uid = UUID.randomUUID().toString()
                        newProperty.postDate = Date()
                        PropertyBank.mProperties.add(newProperty)*/
                        val intent = Intent().also { it.putExtra("status", newProperty.status) }
                        setResult(RESULT_OK, intent)
                        finish()
                        
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this, "An error occurred while adding post", Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                    }
                /*PropertiesHelper.createProperty(newProperty)
                    .addOnSuccessListener {
                        //TODO Tests purposes
                        /*newProperty.uid = UUID.randomUUID().toString()
                        newProperty.postDate = Date()
                        PropertyBank.mProperties.add(newProperty)*/
                        val intent = Intent().also { it.putExtra("status", newProperty.status) }
                        setResult(RESULT_OK, intent)
                        finish()
                        
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this, "An error occurred while adding post", Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                    }*/
                
                
            }
            ?.addOnFailureListener { e ->
                Toast.makeText(this, "An error occurred while loading post's images", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
    }
    
    private interface OnUploadImageCallbacks {
        //Can add onProgress after
        fun onSuccess(index: Int)
        fun onFailed(index: Int, e: Exception?)
    }
    
    
    private fun uploadPropertyImages(
        propertyUid: String,
        imagePathsIterator: ListIterator<String>,
        onUploadImageCallbacks: OnUploadImageCallbacks? = null
    ): Task<UploadTask.TaskSnapshot>? {
        return if (imagePathsIterator.hasNext()) {
            val index = imagePathsIterator.nextIndex()
            val path = imagePathsIterator.next()
            
            //TODO Manage exceptions
            val imageBitmap = BitmapFactory.decodeFile(path)!!
            val finalImageFile = File.createTempFile(UUID.randomUUID().toString(), "jpg")
            //TODO Check for true
            val result = imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, FileOutputStream(finalImageFile))
            
            val fileUri = Uri.fromFile(finalImageFile)
            //val fileUri = Uri.fromFile(File(path))
            //val extension = File(path).extension
            //val fileName = if (extension.isNotEmpty()) "$index.$extension" else "$index"
            val storage = FirebaseStorage.getInstance()
            //val fileRef = storage.reference.child("properties/temp/$propertyUid/images/$fileName")
            val fileRef = storage.reference.child("properties/temp/$propertyUid/images/$index.jpg")
            
            val uploadTask = fileRef.putFile(fileUri)
            return if (imagePathsIterator.hasNext()) {
                uploadTask.continueWithTask { task ->
                    if (!task.isSuccessful) {
                        onUploadImageCallbacks?.onFailed(index, task.exception)
                        task.exception?.let { throw it }
                    }
                    onUploadImageCallbacks?.onSuccess(index)
                    uploadPropertyImages(propertyUid, imagePathsIterator)
                }
            }
            else uploadTask
            
        } else null
    }
    
    
    private fun validateFields(): Boolean {
        var result = true
        var errorAbove = false
        
        citySpinner.errorText = null
        locationInCityAutoComplete.error = null
        typeOfPropertySpinner.errorText = null
        editShortDescription.error = null
        richEditDetailedDescription.error = null
        
        if (selectedCity == null) {
            citySpinner.errorText = "Please choose the city"
            citySpinner.requestFocus()
            errorAbove = true
            result = false
        } else if (TextUtils.isEmpty(locationInCityAutoComplete.text)) {
            locationInCityAutoComplete.error = "Please specify the location in city"
            if (!errorAbove) {
                result = false
                locationInCityAutoComplete.requestFocus()
                errorAbove = true
            }
            
        }
        
        if (typeOfProperty == null) { //Nothing to inspect
            typeOfPropertySpinner.errorText = "You must specify the type of property"
            if (!errorAbove) {
                result = false
                typeOfPropertySpinner.requestFocus()
                errorAbove = true
            }
        }
        
        if (mImageChooser.getImagesPaths() == null) {
            if (!errorAbove) {
                result = false
                mImageChooser.requestFocus()
                errorAbove = true
            }
        }
        
        if (TextUtils.isEmpty(editShortDescription.text)) {
            editShortDescription.error = "You must enter property's short description"
            if (!errorAbove) {
                result = false
                editShortDescription.requestFocus()
                errorAbove = true
            }
        }
        
        if (TextUtils.isEmpty(richEditDetailedDescription.text?.trim())) {
            richEditDetailedDescription.error = "You must enter property's short description"
            if (!errorAbove) {
                result = false
                richEditDetailedDescription.requestFocus()
            }
        }
        
        return result
    }
    
    private fun getBuildingSpecificities(): List<Property.BuildingSpecificityData> {
        val buildingSpecificities = HashSet<Property.BuildingSpecificityData>()
        Property.BuildingSpecificity.values().forEach { specificity ->
            when (specificity) {
                Property.BuildingSpecificity.FENCED -> {
                    buildingSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) buildingSpecificities.add(
                            Property.BuildingSpecificityData.Fenced()
                        )
                    }
                    
                }
                Property.BuildingSpecificity.PARKING -> {
                    buildingSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) buildingSpecificities.add(
                            Property.BuildingSpecificityData.Parking()
                        )
                    }
                    
                }
                Property.BuildingSpecificity.GARAGES -> {
                    buildingSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) buildingSpecificities.add(
                            Property.BuildingSpecificityData.Garages(it.qty)
                        )
                    }
                    
                }
            }
        }
        return buildingSpecificities.toList()
    }
    
    private fun getLandOrFarmSpecificities(): List<Property.LandOrFarmSpecificityData> {
        val landOrFarmSpecificities = HashSet<Property.LandOrFarmSpecificityData>()
        Property.LandOrFarmSpecificity.values().forEach { specificity ->
            when (specificity) {
                Property.LandOrFarmSpecificity.ACCESS_TO_ELECTRICITY -> {
                    landOrFarmSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) landOrFarmSpecificities.add(
                            Property.LandOrFarmSpecificityData.AccessToElectricity()
                        )
                    }
                    
                }
                Property.LandOrFarmSpecificity.ACCESS_TO_WATER -> {
                    landOrFarmSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) landOrFarmSpecificities.add(
                            Property.LandOrFarmSpecificityData.AccessToWater()
                        )
                    }
                    
                }
                Property.LandOrFarmSpecificity.FENCED -> {
                    landOrFarmSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) landOrFarmSpecificities.add(
                            Property.LandOrFarmSpecificityData.Fenced()
                        )
                    }
                    
                }
            }
        }
        return landOrFarmSpecificities.toList()
    }
    
    private fun getHabitationSpecificities(): List<Property.HabitationSpecificityData> {
        val habitationSpecificities = HashSet<Property.HabitationSpecificityData>()
        Property.HabitationSpecificity.values().forEach { specificity ->
            when (specificity) {
                Property.HabitationSpecificity.FURNISHED -> {
                    habitationSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) habitationSpecificities.add(
                            Property.HabitationSpecificityData.Furnished()
                        )
                    }
                    
                }
                Property.HabitationSpecificity.ACCESS_TO_ELECTRICITY -> {
                    habitationSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) habitationSpecificities.add(
                            Property.HabitationSpecificityData.AccessToElectricity()
                        )
                    }
                    
                }
                Property.HabitationSpecificity.ACCESS_TO_WATER -> {
                    habitationSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) habitationSpecificities.add(
                            Property.HabitationSpecificityData.AccessToWater()
                        )
                    }
                    
                }
                Property.HabitationSpecificity.AIR_CONDITIONED -> {
                    habitationSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) habitationSpecificities.add(
                            Property.HabitationSpecificityData.AirConditioned()
                        )
                    }
                    
                }
                Property.HabitationSpecificity.FENCED -> {
                    habitationSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) habitationSpecificities.add(
                            Property.HabitationSpecificityData.Fenced()
                        )
                    }
                    
                }
                Property.HabitationSpecificity.GARAGES -> {
                    habitationSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) habitationSpecificities.add(
                            Property.HabitationSpecificityData.Garages(it.qty)
                        )
                    }
                    
                }
                Property.HabitationSpecificity.SWIMMING_POOLS -> {
                    habitationSpecificitiesMultiToggle.getToggle(specificity.name)?.let {
                        if (it.toggleButton.isChecked) habitationSpecificities.add(
                            Property.HabitationSpecificityData.SwimmingPools(it.qty)
                        )
                    }
                    
                }
            }
        }
        return habitationSpecificities.toList()
    }
    
    
}
package com.ouedyan.immofind.owners

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.ouedyan.immofind.SelectableItemRecyclerViewAdapter
import com.ouedyan.immofind.model.Property
import com.ouedyan.immofind.owners.firestoreapi.PropertiesHelper
import com.scwang.smart.refresh.layout.SmartRefreshLayout

class PostsFragment(val status: Property.PropertyStatus) : Fragment() {
    lateinit var recyclerView: RecyclerView
    lateinit var recyclerViewAdapter: PropertyAdapter
    lateinit var smartRefreshLayout: SmartRefreshLayout
    var onPostsLoadingEndListener: OnPostsLoadingEndListener? = null
    
    private lateinit var onSelectActionModeCallback: ActionMode.Callback
    
    
    /**
     * @return *null* if no loading was yet done
     */
    var isPostsLoading: Boolean? = null
    var onFragmentViewCreatedListener: OnFragmentViewCreatedListener? = null
    
    private val properties = ArrayList<Property>()
    private var lastVisibleProperty: DocumentSnapshot? = null
    
    interface OnFragmentViewCreatedListener {
        fun onViewCreated(view: View)
    }
    
    interface OnPostsLoadingEndListener {
        fun onLoadingEnd(status: Property.PropertyStatus, success: Boolean, startedAfterLastVisible: Boolean, loadedCount: Int)
    }
    
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        
        val layout = inflater.inflate(R.layout.posts_fragment_layout, container, false)
        
        recyclerView = layout.findViewById(R.id.posts_recycler_view)
        smartRefreshLayout = layout.findViewById(R.id.posts_smart_refresh)
        
        //posts adapter
        recyclerViewAdapter = PropertyAdapter(properties, inflater.context)
        recyclerViewAdapter.isSelectable = true
        
        onSelectActionModeCallback = object : ActionMode.Callback {
            
            override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                smartRefreshLayout.setEnableRefresh(false)
                mode.menuInflater.inflate(R.menu.select_menu, menu)
                return true
            }
            
            override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                return false
            }
            
            override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                when (item.itemId) {
                    
                    R.id.set_as_acquired -> {
                        //
                        mode.finish()
                        return true
                    }
                    
                    R.id.set_as_unavailable -> {
                        //
                        mode.finish()
                        return true
                    }
                    
                    R.id.delete_post -> {
                        deletePostsAt(*recyclerViewAdapter.selectedItems.toIntArray())
                        mode.finish()
                        return true
                    }
                }
                
                return false
            }
            
            override fun onDestroyActionMode(mode: ActionMode) {
                recyclerViewAdapter.clearSelection()
                onSelectActionMode = null
                smartRefreshLayout.setEnableRefresh(true)
            }
        }
        
        recyclerViewAdapter.onItemSelectListener = object : SelectableItemRecyclerViewAdapter.OnItemSelectListener {
            override fun onItemSelect(position: Int, itemView: View, cancellation: Boolean) {
                activity?.let {
                    if (it is AppCompatActivity) {
                        val activity = it
                        if (!cancellation) {
                            val isFirstSelected =
                                recyclerViewAdapter.selectedItems.contains(position) && recyclerViewAdapter.selectedItems.size == 1
                            if (isFirstSelected) {
                                onSelectActionMode = activity.startSupportActionMode(onSelectActionModeCallback)
                            }
                            onSelectActionMode!!.title = recyclerViewAdapter.selectedItems.size.toString()
                            onSelectActionMode!!.invalidate()
                            
                        } else {
                            if (recyclerViewAdapter.selectedItems.isNotEmpty()) {
                                onSelectActionMode!!.title = recyclerViewAdapter.selectedItems.size.toString()
                                onSelectActionMode!!.invalidate()
                            } else {
                                onSelectActionMode!!.finish()
                                onSelectActionMode = null
                            }
                            
                        }
                        
                    }
                }
                
            }
            
            override fun onItemSelectViewState(position: Int, itemView: View, cancellation: Boolean) {
                val itemLayout = itemView as MaterialCardView
                if (!cancellation) {
                    itemLayout.foreground = ContextCompat.getDrawable(inflater.context, R.color.dimmed_black)
                } else {
                    itemLayout.foreground = ContextCompat.getDrawable(inflater.context, R.drawable.ripple_foreground)
                }
            }
            
        }
        
        recyclerView.adapter = recyclerViewAdapter
        
        smartRefreshLayout.setOnRefreshListener {
            reloadPosts()
        }
        
        val firebaseAuth = FirebaseAuth.getInstance()
        firebaseAuth.addAuthStateListener {
            it.currentUser?.let {
                loadPosts(status)
                //firebaseAuth.removeAuthStateListener(this)
            }
        }
        
        return layout
    }
    
    private fun deletePostsAt(vararg positions: Int) {
        if (positions.isNotEmpty()){
            isPostsLoading = true
            smartRefreshLayout.autoRefreshAnimationOnly()
            val selectedItemsIds = arrayListOf<String>()
            positions.forEach { position ->
                selectedItemsIds.add(properties[position].uid)
            }
            PropertiesHelper.deleteProperties(*selectedItemsIds.toTypedArray())
                .addOnSuccessListener {
                    positions.forEach {
                        properties.removeAt(it)
                        recyclerViewAdapter.notifyItemRemoved(it)
                    }
                    isPostsLoading = false
                    smartRefreshLayout.finishRefresh()
                    Toast.makeText(requireContext(), "${positions.size} posts removed.", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener { e ->
                    Log.e("deletePostsAt", e.stackTraceToString())
                    isPostsLoading = false
                    smartRefreshLayout.finishRefresh()
                    Toast.makeText(requireContext(), "Error while removing ${positions.size} posts.", Toast.LENGTH_SHORT).show()
            
                }
    
        }
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onFragmentViewCreatedListener?.onViewCreated(view)
        
    }
    
    fun reloadPosts() {
        loadPosts(status)
    }
    
    private fun loadPosts(status: Property.PropertyStatus, startAfterLastVisible: Boolean = false) {
        isPostsLoading = true
        PropertiesHelper.getProperties(status, if (startAfterLastVisible) lastVisibleProperty else null)
            .addOnSuccessListener { documentSnapshots ->
                if (!startAfterLastVisible) {
                    properties.clear()
                    lastVisibleProperty = null
                    recyclerViewAdapter.notifyDataSetChanged()
                }
                var loadedCount = 0
                if (!documentSnapshots.isEmpty) {
                    documentSnapshots.forEach {
                        properties.add(it.toObject(Property::class.java))
                        recyclerViewAdapter.notifyItemInserted(properties.size - 1)
                        loadedCount++
                    }
                    lastVisibleProperty = documentSnapshots.documents[documentSnapshots.size() - 1]
                    
                }
                smartRefreshLayout.finishRefresh(true)
                isPostsLoading = false
                onPostsLoadingEndListener?.onLoadingEnd(status, true, startAfterLastVisible, loadedCount)
            }
            .addOnFailureListener { e ->
                Log.e("loadFirstPosts", e.stackTraceToString())
                view?.let {
                    Snackbar.make(it, "Error while retrieving posts", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry") { loadPosts(status) }
                        .show()
                }
                smartRefreshLayout.finishRefresh(false)
                isPostsLoading = false
                onPostsLoadingEndListener?.onLoadingEnd(status, false, startAfterLastVisible, 0)
            }
    }
    
    
    companion object {
        var onSelectActionMode: ActionMode? = null
        
    }
    
}
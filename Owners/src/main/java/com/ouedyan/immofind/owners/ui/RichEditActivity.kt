package com.ouedyan.immofind.owners.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.chinalwb.are.AREditText
import com.chinalwb.are.styles.toolbar.IARE_Toolbar
import com.chinalwb.are.styles.toolitems.*
import com.ouedyan.immofind.owners.R

class RichEditActivity : AppCompatActivity() {
    private lateinit var iAreToolbar: IARE_Toolbar
    private lateinit var editText: AREditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rich_edit)
        supportActionBar?.title = intent.getStringExtra("title")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        editText = findViewById(R.id.arEditText)
        editText.hint = intent.getStringExtra("hint")
        editText.fromHtml(intent.getStringExtra("html"))
        setupToolbar()
    }

    private fun setupToolbar() {
        iAreToolbar = findViewById(R.id.areToolbar)
        iAreToolbar.addToolbarItem(ARE_ToolItem_Bold())
        iAreToolbar.addToolbarItem(ARE_ToolItem_Underline())
        iAreToolbar.addToolbarItem(ARE_ToolItem_Italic())
        iAreToolbar.addToolbarItem(ARE_ToolItem_ListBullet())
        iAreToolbar.addToolbarItem(ARE_ToolItem_ListNumber())
        iAreToolbar.addToolbarItem(ARE_ToolItem_AlignmentLeft())
        iAreToolbar.addToolbarItem(ARE_ToolItem_AlignmentCenter())
        iAreToolbar.addToolbarItem(ARE_ToolItem_AlignmentRight())
        iAreToolbar.addToolbarItem(ARE_ToolItem_FontSize())
        iAreToolbar.addToolbarItem(ARE_ToolItem_Hr())
        iAreToolbar.addToolbarItem(ARE_ToolItem_BackgroundColor())
        iAreToolbar.addToolbarItem(ARE_ToolItem_FontColor())
        iAreToolbar.addToolbarItem(ARE_ToolItem_At())
        iAreToolbar.addToolbarItem(ARE_ToolItem_Subscript())
        iAreToolbar.addToolbarItem(ARE_ToolItem_Superscript())
        iAreToolbar.addToolbarItem(ARE_ToolItem_Strikethrough())
        editText.textSize = 15f
        editText.setToolbar(iAreToolbar)
    }

    private fun setDefaultHtml() {
        val html = """
            <p style="text-align: center;"><strong>New Feature in 0.1.2</strong></p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: left;"><span style="color: #3366ff;">In this release, you have a new usage with ARE.</span></p>
            <p style="text-align: left;">&nbsp;</p>
            <p style="text-align: left;"><span style="color: #3366ff;">AREditText + ARE_Toolbar, you are now able to control the position of the input area and where to put the toolbar at and, what ToolItems you'd like to have in the toolbar. </span></p>
            <p style="text-align: left;">&nbsp;</p>
            <p style="text-align: left;"><span style="color: #3366ff;">You can not only define the Toolbar (and it's style), you can also add your own ARE_ToolItem with your style into ARE.</span></p>
            <p style="text-align: left;">&nbsp;</p>
            <p style="text-align: left;"><span style="color: #ff00ff;"><em><strong>Why not give it a try now?</strong></em></span></p>
            """.trimIndent()
        editText.fromHtml(html)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun done(view: View?) {
        val intent = Intent()
        intent.putExtra("html", editText.html)
        setResult(RESULT_OK, intent)
        finish()
    }
}
package com.ouedyan.immofind.owners.ui

import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import android.os.Bundle
import com.ouedyan.immofind.owners.R
import com.google.firebase.auth.FirebaseAuth
import android.widget.TextView.OnEditorActionListener
import android.view.inputmethod.EditorInfo
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.ouedyan.immofind.Utils
import com.ouedyan.splashscreen.SplashScreen
import java.util.*

class   CompleteSignUpActivity : AppCompatActivity() {
    private lateinit var editDenomination: TextInputEditText
    private lateinit var editFirstName: TextInputEditText
    private lateinit var editLastName: TextInputEditText
    private lateinit var signUpProgress: SplashScreen
    private lateinit var rootLayout: ScrollView
    lateinit var email: String
    lateinit var denomination: String
    lateinit var firstName: String
    lateinit var lastName: String
    private var isBusiness = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_sign_up)

        //View Binding
        rootLayout = findViewById(R.id.rootLayout)
        signUpProgress = findViewById(R.id.progress)
        val radioIndividualOrBusiness = findViewById<RadioGroup>(R.id.radio_individual_or_business)
        editDenomination = findViewById(R.id.edit_denomination)
        editFirstName = findViewById(R.id.edit_first_name)
        editLastName = findViewById(R.id.edit_last_name)
        val editEmail = findViewById<TextInputEditText>(R.id.edit_txt_Email)
        val buttonRegister = findViewById<Button>(R.id.button_register)

        //Get Firebase auth instance
        val mAuth = FirebaseAuth.getInstance()

        //Displaying specific fields whether they are individuals or businesses
        radioIndividualOrBusiness.clearCheck()
        radioIndividualOrBusiness.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.radio_individual) {
                clearErrors()
                findViewById<View>(R.id.field_denomination).visibility = View.GONE
                findViewById<View>(R.id.field_first_name).visibility = View.VISIBLE
                findViewById<View>(R.id.field_last_name).visibility = View.VISIBLE
                isBusiness = false
            } else {
                clearErrors()
                findViewById<View>(R.id.field_denomination).visibility = View.VISIBLE
                findViewById<View>(R.id.field_first_name).visibility = View.GONE
                findViewById<View>(R.id.field_last_name).visibility = View.GONE
                isBusiness = true
            }
        }
        radioIndividualOrBusiness.check(R.id.radio_individual) //Individual checked by default
        email = mAuth.currentUser!!.email!!
        editEmail.setText(email)
        editEmail.isEnabled = false

        //Set TOS o-r Privacy Policy urls
        val tosAndPp = findViewById<TextView>(R.id.text_tos_pp)
        val tosText = getString(R.string.tos_text)
        val ppText = getString(R.string.pp_text)
        val wholeText = getString(R.string.tos_and_pp_text, tosText, ppText)
        val tosUrl = getString(R.string.tosUrl)
        val ppUrl = getString(R.string.privacyUrl)
        val textColor = ContextCompat.getColor(this, R.color.colorAccent)
        Utils.setTosAndPpTextView(this, tosAndPp, wholeText, tosText, ppText, tosUrl, ppUrl, textColor)
        val onEditorActionListener = OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) signUp()
            false
        }
        editDenomination.setOnEditorActionListener(onEditorActionListener)
        editLastName.setOnEditorActionListener(onEditorActionListener)

        //Handle user SignUp button
        buttonRegister.setOnClickListener { signUp() }
    }

    private fun clearErrors() {
        editFirstName.error = null
        editDenomination.error = null
        editLastName.error = null
    }

    private fun signUp() {
        if (!validateFields()) return

        //progressbar VISIBLE
        signUpProgress.show = true
        val firebaseAuth = FirebaseAuth.getInstance()
        val newOwner = com.ouedyan.immofind.owners.model.Owner()
        //Get owner entered details
        newOwner.isBusiness = isBusiness
        newOwner.email = email
        newOwner.denomination = if (isBusiness) denomination else "$firstName $lastName"
        //Save Owner in Firestore
        com.ouedyan.immofind.owners.firestoreapi.OwnersHelper.createOwner(newOwner,
                firebaseAuth.currentUser!!.uid)
                .addOnSuccessListener { //signUpProgress.show(false);
                    Toast.makeText(this@CompleteSignUpActivity, "Successfully Registered",
                            Toast.LENGTH_SHORT).show()
                    setResult(RESULT_OK)
                    finish()
                }
                .addOnFailureListener { e ->
                    signUpProgress.show = false
                    Snackbar.make(rootLayout, "Error while registering you to the database",
                            Snackbar.LENGTH_SHORT).show()
                    //Timber.e(e);
                    e.printStackTrace()
                }
    }

    private fun validateFields(): Boolean {
        var result = true
        if (isBusiness) {
            denomination = editDenomination.text.toString().trim { it <= ' ' }
            if (android.text.TextUtils.isEmpty(denomination)) {
                editDenomination.error = "Please enter your business' denomination"
                result = false
            }
        } else { //Individual
            firstName = editFirstName.text.toString().trim { it <= ' ' }
            lastName = editLastName.text.toString().trim { it <= ' ' }
            if (android.text.TextUtils.isEmpty(firstName)) {
                editFirstName.error = "Please enter your first name"
                result = false
            }
            if (android.text.TextUtils.isEmpty(lastName)) {
                editLastName.error = "Please enter your last name"
                result = false
            }
        }
        return result
    }

}

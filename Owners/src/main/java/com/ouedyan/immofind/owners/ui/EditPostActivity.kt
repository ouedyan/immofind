package com.ouedyan.immofind.owners.ui

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.chinalwb.are.AREditText
import com.chivorn.smartmaterialspinner.SmartMaterialSpinner
import com.google.android.material.slider.Slider
import com.ouedyan.imagechooser.ImageChooser
import com.ouedyan.immofind.model.City
import com.ouedyan.immofind.Location
import com.ouedyan.immofind.owners.R
import com.ouedyan.immofind.Utils.dpToPx
import com.ouedyan.immofind.model.Property
import it.sephiroth.android.library.numberpicker.NumberPicker
import it.sephiroth.android.library.numberpicker.doOnProgressChanged
import java.util.*

class EditPostActivity : AppCompatActivity() {
    private val richTextRequestCode = 1
    private lateinit var res: Resources

    //TODO Get country from settings
    private val currentCountry = "BF"

    private lateinit var locationInCity: AutoCompleteTextView
    private lateinit var richEditDetailedDescription: AREditText
    private lateinit var mImageChooser: ImageChooser

    private var typeOfProperty: Property.TypeOfProperty? = null
    private var landOrFarmLandSurfaceUnit: Property.AreaUnit? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_post)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mImageChooser = findViewById(R.id.imagechooser)
        res = resources

        //City spinner setup
        val citySpinner = findViewById<SmartMaterialSpinner<String>>(R.id.spinner_city)
        val cities = ArrayList<City>()
        Location.getCountry(currentCountry)?.let {
            cities.addAll(it.cities)
            val citiesNames = ArrayList<String>()
            for (city in cities) citiesNames.add(city.name)
            citySpinner.item = citiesNames
        }


        //Enabling location in city auto complete editText according to city is selected or not
        locationInCity = findViewById(R.id.auto_complete_location_in_city)
        locationInCity.inputType = EditorInfo.TYPE_NULL
        citySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                locationInCity.apply {
                    isEnabled = true
                    text = null
                    inputType = EditorInfo.TYPE_TEXT_VARIATION_SHORT_MESSAGE
                    inputType = EditorInfo.TYPE_TEXT_FLAG_CAP_WORDS
                    val sectors = ArrayList<String>()
                    cities[position].numberOfSectors?.let {
                        if (it > 1)
                            for (i in 1..it) sectors.add(getString(R.string.sector) + " $i")
                    }
                    val neighborhoods = ArrayList<String>()
                    cities[position].neighborhoods?.let { neighborhoods += it }
                    setAdapter(ArrayAdapter(
                            this@EditPostActivity,
                            android.R.layout.simple_dropdown_item_1line,
                            (neighborhoods + sectors)))
                    requestFocus()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                locationInCity.text = null
                locationInCity.inputType = EditorInfo.TYPE_NULL
                locationInCity.isEnabled = false
            }
        }

        //Clearing focus from location in city auto complete editText after a suggestion has been selected
        //(note : OnItemSelectedListener doesn't work)
        locationInCity.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            locationInCity.onEditorAction(EditorInfo.IME_ACTION_DONE)
        }

        //Type of property spinner setup
        val typeOfPropertyList = Property.TypeOfProperty.values()
        //Getting strings
        val typeOfPropertyStringList = ArrayList<String>()
        typeOfPropertyList.forEach { typeOfPropertyStringList.add(getString(it.stringRes)) }
        //Setting spinner
        val typeOfPropertySpinner = findViewById<SmartMaterialSpinner<String>>(R.id.spinner_type_of_property)
        typeOfPropertySpinner.item = typeOfPropertyStringList
        typeOfPropertySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                val selected = typeOfPropertyList[position]
                if (typeOfProperty != selected) {
                    typeOfProperty = selected
                    onTypeOfPropertySelected(selected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                typeOfProperty = null
                onTypeOfPropertySelected(null)
            }
        }

        //Surface unit chooser spinner setup
        val areaUnitList = Property.AreaUnit.values()
        //Getting strings
        val areaUnitStringList = ArrayList<String>()
        areaUnitList.forEach { areaUnitStringList.add(getString(it.stringRes)) }

        val surfaceUnitSpinner = findViewById<SmartMaterialSpinner<String>>(R.id.spinner_land_surface_unit_land_or_farm)
        surfaceUnitSpinner.item = areaUnitStringList
        surfaceUnitSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                val selected = areaUnitList[position]
                if (landOrFarmLandSurfaceUnit != selected) {
                    landOrFarmLandSurfaceUnit = selected
                    val numberPicker = findViewById<NumberPicker>(R.id.number_picker_land_surface_land_or_farm)
                    val slider = findViewById<Slider>(R.id.slider_land_surface_land_or_farm)
                    when (selected) {
                        Property.AreaUnit.SQUARE_METERS -> {
                            numberPicker.minValue = res.getInteger(R.integer.land_or_farm_min_land_surface_m2).toDouble()
                            numberPicker.maxValue = res.getInteger(R.integer.land_or_farm_max_land_surface_m2).toDouble()
                            numberPicker.setProgress(res.getInteger(R.integer.land_or_farm_default_land_surface_m2).toBigDecimal())

                            slider.valueFrom = res.getInteger(R.integer.land_or_farm_min_land_surface_m2).toFloat()
                            slider.valueTo = res.getInteger(R.integer.land_or_farm_max_land_surface_m2).toFloat()
                            //Don't forget when min and max set programmatically or error
                            slider.value = res.getInteger(R.integer.land_or_farm_default_land_surface_m2).toFloat()
                        }
                        Property.AreaUnit.HECTARES -> {
                            numberPicker.minValue = res.getInteger(R.integer.land_or_farm_min_land_surface_ha).toDouble()
                            numberPicker.maxValue = res.getInteger(R.integer.land_or_farm_max_land_surface_ha).toDouble()
                            numberPicker.setProgress(res.getInteger(R.integer.land_or_farm_default_land_surface_ha).toBigDecimal())

                            slider.valueFrom = res.getInteger(R.integer.land_or_farm_min_land_surface_ha).toFloat()
                            slider.valueTo = res.getInteger(R.integer.land_or_farm_max_land_surface_ha).toFloat()
                            //Don't forget when min and max set programmatically or error
                            slider.value = res.getInteger(R.integer.land_or_farm_default_land_surface_ha).toFloat()
                        }
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                landOrFarmLandSurfaceUnit = null
                //TODO Impossible to not choose
            }
        }
        surfaceUnitSpinner.setSelection(1) //Default unit selection


        //Min and max values of type of properties' characteristics setting in accordance to selected
        // status(for rent or sale)
        val radioGroupStatus = findViewById<RadioGroup>(R.id.radio_rent_or_sale)
        radioGroupStatus.clearCheck()
        radioGroupStatus.check(R.id.radio_rent) // Check for rent by default
        linkSlidersAndNumberPickers()

        //Adding clearing focus of the editor to the default #onEditorAction() behavior.
        val customOnEditorActionListener = OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                v.clearFocus()
                hideKeyboard()
            }
            false
        }
        val editShortDescription = findViewById<EditText>(R.id.edit_short_description)
        editShortDescription.setOnEditorActionListener(customOnEditorActionListener)
        locationInCity.setOnEditorActionListener(customOnEditorActionListener)

        //Wrapping lines manually instead of automatically(putting textMultiline flag on inputType) on simple
        // editText, because textMultiline displays Enter at imeAction DONE place .
        // Setting it from xml doesn't work
        editShortDescription.setHorizontallyScrolling(false)
        editShortDescription.maxLines = 4


        //Detailed description rich text editor styling
        richEditDetailedDescription = findViewById(R.id.rich_edit_detailed_description)
        richEditDetailedDescription.textSize = 14f
        richEditDetailedDescription.background = ContextCompat.getDrawable(this, android.R.color.transparent)
        richEditDetailedDescription.isFocusable = false
        richEditDetailedDescription.isFocusableInTouchMode = false
        richEditDetailedDescription.setPadding(
                dpToPx(resources, 20),
                dpToPx(resources, 7),
                dpToPx(resources, 20),
                dpToPx(resources, 7))
    }

    fun openRichTextEditor(view: View?) {
        val html = richEditDetailedDescription.html
        val intent = Intent(this, RichEditActivity::class.java)
        //TODO Retrieve values from resources for translation
        intent.putExtra("title", "Detailed description")
        intent.putExtra("hint", "Enter property's detailed description here...")
        intent.putExtra("html", html)
        startActivityForResult(intent, richTextRequestCode)
    }

    private fun linkSlidersAndNumberPickers() {
        //Habitation
        val numberPickerSurfaceHabitation = findViewById<NumberPicker>(R.id.number_picker_surface_habitation)
        val sliderSurfaceHabitation = findViewById<Slider>(R.id.slider_surface_habitation)
        linkSliderAndNumberPicker(sliderSurfaceHabitation, numberPickerSurfaceHabitation)
        val numberPickerLandSurfaceHabitation = findViewById<NumberPicker>(R.id.number_picker_land_surface_habitation)
        val sliderLandSurfaceHabitation = findViewById<Slider>(R.id.slider_land_surface_habitation)
        linkSliderAndNumberPicker(sliderLandSurfaceHabitation, numberPickerLandSurfaceHabitation)
        val numberPickerRoomsHabitation = findViewById<NumberPicker>(R.id.number_picker_rooms_habitation)
        val sliderRoomsHabitation = findViewById<Slider>(R.id.slider_rooms_habitation)
        linkSliderAndNumberPicker(sliderRoomsHabitation, numberPickerRoomsHabitation)
        val numberPickerBedroomsHabitation = findViewById<NumberPicker>(R.id.number_picker_bedrooms_habitation)
        val sliderBedroomsHabitation = findViewById<Slider>(R.id.slider_bedrooms_habitation)
        linkSliderAndNumberPicker(sliderBedroomsHabitation, numberPickerBedroomsHabitation)

        //Land or Farm
        val numberPickerLandSurfaceLandOrFarm = findViewById<NumberPicker>(R.id.number_picker_land_surface_land_or_farm)
        val sliderLandSurfaceLandOrFarm = findViewById<Slider>(R.id.slider_land_surface_land_or_farm)
        linkSliderAndNumberPicker(sliderLandSurfaceLandOrFarm, numberPickerLandSurfaceLandOrFarm)
        //Building
        val numberPickerSurfaceBuilding = findViewById<NumberPicker>(R.id.number_picker_surface_building)
        val sliderSurfaceBuilding = findViewById<Slider>(R.id.slider_surface_building)
        linkSliderAndNumberPicker(sliderSurfaceBuilding, numberPickerSurfaceBuilding)
        val numberPickerRoomsBuilding = findViewById<NumberPicker>(R.id.number_picker_rooms_building)
        val sliderRoomsBuilding = findViewById<Slider>(R.id.slider_rooms_building)
        linkSliderAndNumberPicker(sliderRoomsBuilding, numberPickerRoomsBuilding)
        //Office rooms
        val numberPickerOfficeRooms = findViewById<NumberPicker>(R.id.number_picker_office_rooms_office_rooms)
        val sliderOfficeRooms = findViewById<Slider>(R.id.slider_office_rooms_office_rooms)
        linkSliderAndNumberPicker(sliderOfficeRooms, numberPickerOfficeRooms)
    }

    private fun linkSliderAndNumberPicker(slider: Slider, numberPicker: NumberPicker) {
        numberPicker.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            slider.value = progress.toFloat()
        }
        slider.addOnChangeListener { thisSlider, value, fromUser ->
            numberPicker.setProgress(thisSlider.value.toBigDecimal().stripTrailingZeros())
        }
    }

    private fun linkSliderAndNumberPicker(slider: Slider, numberPicker: NumberPicker, minValue: Int, maxValue: Int) {
        //TO review computing default value algorithm
        numberPicker.minValue = minValue.toDouble()
        numberPicker.maxValue = maxValue.toDouble()
        numberPicker.setProgress((minValue + maxValue shr 1).toBigDecimal())
        //TO see result
        slider.valueFrom = minValue.toFloat()
        slider.valueTo = maxValue.toFloat()
        slider.value = (minValue + maxValue shr 1).toFloat()
        numberPicker.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            slider.value = progress.toFloat()
        }
        slider.addOnChangeListener { thisSlider, value, fromUser ->
            numberPicker.setProgress(thisSlider.value.toBigDecimal().stripTrailingZeros())
        }
    }

    private fun setupLinkedSliderAndNumberPickerDefaultValues(
            slider: Slider, numberPicker: NumberPicker,
            minValue: Int,
            defaultValue: Int,
            maxValue: Int) {
        numberPicker.maxValue = maxValue.toDouble()
        numberPicker.minValue = minValue.toDouble() //A bit shit : mandatory to set maxValue value
        // before minValue value in order for minValue to not be greater than maxValue original value
        numberPicker.setProgress(defaultValue.toBigDecimal())

        slider.valueFrom = minValue.toFloat()
        slider.valueTo = maxValue.toFloat()
        slider.value = defaultValue.toFloat()
    }

    private fun onTypeOfPropertySelected(typeOfProperty: Property.TypeOfProperty?) {
        val habitationBlock = findViewById<View>(R.id.habitation_block)
        val landOrFarmBlock = findViewById<View>(R.id.land_or_farm_block)
        val buildingBlock = findViewById<View>(R.id.building_block)
        val officeRoomsBlock = findViewById<View>(R.id.office_rooms_block)
        val allBlocks = ArrayList(listOf(
                habitationBlock,
                landOrFarmBlock,
                buildingBlock,
                officeRoomsBlock
        ))
        for (block in allBlocks) block.visibility = View.GONE
        if (typeOfProperty != null) when (typeOfProperty) {
            Property.TypeOfProperty.HABITATION -> habitationBlock.visibility = View.VISIBLE
            Property.TypeOfProperty.LAND_OR_LAND_LOT, Property.TypeOfProperty.FARM -> landOrFarmBlock.visibility = View.VISIBLE
            Property.TypeOfProperty.BUILDING -> buildingBlock.visibility = View.VISIBLE
            Property.TypeOfProperty.OFFICE_ROOMS -> officeRoomsBlock.visibility = View.VISIBLE
            Property.TypeOfProperty.COMMERCIAL_PLACE, Property.TypeOfProperty.STORE, Property.TypeOfProperty.FACTORY -> {
                //Nothing
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == richTextRequestCode) {
                richEditDetailedDescription.text = null //do not append
                richEditDetailedDescription.fromHtml(data.getStringExtra("html"))
            } else if (requestCode == ImageChooser.REQUEST_CODE) {
                mImageChooser.handleImagePickerResult(data, resultCode)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window.decorView.windowToken, 0)
    }


    fun post(view: View?) {
        val newProperty = Property()
        //TODO get all info and save to firestore
        setResult(RESULT_OK)
        finish()
    }

    fun onHabitationSpecificityToggle(view: View?) {}
    fun onLandOrFarmSpecificityToggle(view: View?) {}
    fun onBuildingSpecificityToggle(view: View?) {}

}
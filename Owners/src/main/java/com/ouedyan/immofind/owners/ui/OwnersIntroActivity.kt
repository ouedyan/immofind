package com.ouedyan.immofind.owners.ui

import android.app.Activity
import android.os.Bundle
import com.ouedyan.immofind.owners.R

class OwnersIntroActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_owners_intro)
    }
}
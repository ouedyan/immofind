package com.ouedyan.immofind.owners

import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ouedyan.immofind.model.Property

class PostsViewPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
    val fragments = HashMap<Int, PostsFragment>()
    var onFragmentCreatedListener : OnFragmentCreatedListener? = null

    interface OnFragmentCreatedListener {
        fun onFragmentCreated(position: Int, fragment: PostsFragment)
    }

    override fun getItemCount() = Property.PropertyStatus.values().size

    override fun createFragment(position: Int): PostsFragment {
        val status = Property.PropertyStatus.values()[position]
        val fragment = PostsFragment(status)

        fragment.onFragmentViewCreatedListener = object : PostsFragment.OnFragmentViewCreatedListener{
            override fun onViewCreated(view: View) {
                fragments[position] = fragment
                onFragmentCreatedListener?.onFragmentCreated(position, fragment)
                fragment.onFragmentViewCreatedListener = null
                
            }
        }
        return fragment
    }


}
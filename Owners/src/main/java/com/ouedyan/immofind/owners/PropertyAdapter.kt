package com.ouedyan.immofind.owners

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.flexbox.FlexboxLayout
import com.google.firebase.storage.FirebaseStorage
import com.ouedyan.immofind.SelectableItemRecyclerViewAdapter
import com.ouedyan.immofind.model.Property
import com.ouedyan.immofind.model.Tag
import com.ouedyan.immofind.owners.PropertyAdapter.PropertyViewHolder
import org.ocpsoft.prettytime.PrettyTime
import java.io.File
import java.util.*

class PropertyAdapter(private val mProperties: ArrayList<Property>, private val mContext: Context) :
    SelectableItemRecyclerViewAdapter<PropertyViewHolder>() {
    
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyViewHolder {
        val propertyView = LayoutInflater.from(parent.context).inflate(R.layout.property_view, parent, false)
        return PropertyViewHolder(propertyView)
    }
    
    override fun onBindViewHolder(holder: PropertyViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        
        val currentProperty = mProperties[position]
        
        //TODO Take care of temps with Cloud Functions
        val propertyMainPictureStorageRef =
            FirebaseStorage.getInstance().reference.child("properties/temp/${currentProperty.uid}/images/0.jpg")
        GlideApp.with(mContext)
        //Glide.with(mContext)
            .load(propertyMainPictureStorageRef)
            //.load(File(currentProperty.propertyMainPictureUrl!!))
            .placeholder(R.color.dimmed_white)
            .thumbnail(0.1f)
            .into(holder.propertyMainPicture)
        
        holder.apply {
            date.text = PrettyTime().format(currentProperty.postDate)
            city.text = currentProperty.getCity().name
            street.text = currentProperty.locationInCity
            shortDescription.text = currentProperty.shortDescription
            price.text = currentProperty.getPropertyFormattedPrice(mContext)
        }
        val areaText: String? = currentProperty.getAreaWithUnit(mContext)
        holder.area.text = areaText
        
        //TODO Display's optimisation : recyclerview scrolling
        holder.tagsFlexBox.removeAllViews() //because holder is static
        for (tag in currentProperty.tags) {
            //TODO Create instead of inflating
            val tagView = LayoutInflater.from(mContext).inflate(
                R.layout.tag_view, holder.tagsFlexBox, false
            ) as Button
            
            tagView.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext,
                if (tag.type == Tag.TAG_TYPE.PROPERTY_TYPE_TAG) R.color.colorPrimaryDark else R.color.dark_grey))
            
            var tagText = mContext.getString(
                mContext.resources.getIdentifier(
                    tag.textStringResourceEntryName,
                    "string",
                    mContext.packageName
                )
            )
            if (! tag.isTextOnlyTag) tagText += "    " + tag.quantity
            tagView.text = tagText
            
            /*if (tag.isTextOnlyTag) tagView.text = mContext.getString(
                mContext.resources.getIdentifier(
                    tag.textStringResourceEntryName,
                    "string",
                    mContext.packageName
                )
            )
            else {
                val tagText = mContext.getString(
                    mContext.resources.getIdentifier(
                        tag.textStringResourceEntryName,
                        "string",
                        mContext.packageName
                    )
                ) + "    " + tag.quantity
                tagView.text = tagText
            }*/
            
            holder.tagsFlexBox.addView(tagView)
        }
        
    }
    
    override fun getItemCount(): Int {
        return mProperties.size
    }
    
    class PropertyViewHolder(propertyView: View) : RecyclerView.ViewHolder(propertyView) {
        var propertyMainPicture: ImageView = propertyView.findViewById(R.id.property_main_picture)
        var date: TextView = propertyView.findViewById(R.id.date)
        var city: TextView = propertyView.findViewById(R.id.city)
        var street: TextView = propertyView.findViewById(R.id.street)
        var shortDescription: TextView = propertyView.findViewById(R.id.short_description)
        var price: TextView = propertyView.findViewById(R.id.price)
        var area: TextView = propertyView.findViewById(R.id.area)
        var tagsFlexBox: FlexboxLayout = propertyView.findViewById(R.id.tags_flex_box)
    }
}
package com.ouedyan.immofind.owners.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

class Owner {
    @DocumentId
    lateinit var uid: String
    var isBusiness = false
    lateinit var denomination: String

    @ServerTimestamp
    var registrationDate: Date? = null
    var tel = 0
    lateinit var email: String
    var whatsappTel = 0
    var messengerLink: String? = null
}
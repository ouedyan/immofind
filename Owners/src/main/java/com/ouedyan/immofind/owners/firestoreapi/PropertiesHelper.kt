package com.ouedyan.immofind.owners.firestoreapi

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.ouedyan.immofind.model.Property

object PropertiesHelper {
    const val COLLECTION_NAME = "properties"
    
    // --- COLLECTION REFERENCE ---
    val propertiesCollection: CollectionReference
        get() = FirebaseFirestore.getInstance().collection(COLLECTION_NAME)
    
    // --- CREATE ---
    fun createProperty(newProperty: Property): Task<DocumentReference> {
        return propertiesCollection.add(newProperty)
    }
    
    fun createProperty(newProperty: Property, uid: String): Task<Void> {
        return propertiesCollection.document(uid).set(newProperty)
    }
    
    // --- GET ---
    fun getProperty(uid: String): Task<DocumentSnapshot> {
        return propertiesCollection.document(uid).get()
    }
    
    fun getProperties(status: Property.PropertyStatus, startAfter: DocumentSnapshot? = null): Task<QuerySnapshot> {
        val query = propertiesCollection
            .whereEqualTo("ownerUid", FirebaseAuth.getInstance().currentUser!!.uid)
            .whereEqualTo("status", status)
            .orderBy("postDate", Query.Direction.DESCENDING)
            .limit(15)
        startAfter?.let { query.startAfter(it) }
        
        return query.get()
    }
    
    // --- UPDATE ---
    fun updateProperty(propertyNewData: Property): Task<Void> {
        deleteProperties()
        return propertiesCollection.document(propertyNewData.uid).set(propertyNewData)
    }
    
    // --- DELETE ---
    fun deleteProperties(vararg ids: String): Task<Unit> {
        return FirebaseFirestore.getInstance().runTransaction { transaction ->
            ids.forEach { id ->
                transaction.delete(propertiesCollection.document(id))
            }
        }
    }
}
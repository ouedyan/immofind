package com.ouedyan.immofind.owners.ui

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.widget.NestedScrollView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.viewpager2.widget.ViewPager2
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig.EmailBuilder
import com.firebase.ui.auth.AuthUI.IdpConfig.GoogleBuilder
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.auth.FirebaseAuth
import com.ouedyan.immofind.owners.R
import com.ouedyan.immofind.model.Property
import com.ouedyan.immofind.owners.PostsFragment
import com.ouedyan.immofind.owners.PostsViewPagerAdapter
import com.ouedyan.splashscreen.SplashScreen
import timber.log.Timber
import java.util.*

//TODO Add filter as in Clients Activity but here to filter properties states(available...)
class MainActivityOwner : AppCompatActivity() {
    
    companion object {
        private const val SIGN_IN_REQUEST_CODE = 5
        private const val NEW_POST_REQUEST_CODE = 6
        private const val COMPLETE_SIGN_UP_REQUEST_CODE = 7
    }
    
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var nestedScrollView: NestedScrollView
    private lateinit var tabs: TabLayout
    private lateinit var splashScreen: SplashScreen
    
    private lateinit var postsViewPager: ViewPager2
    private lateinit var postsViewPagerAdapter: PostsViewPagerAdapter
    
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signOwnerIn()
        
        setContentView(R.layout.activity_main_owner)
        
        splashScreen = findViewById(R.id.splash)
        nestedScrollView = findViewById(R.id.nested_scrollview)
        
        splashScreen.visibility = View.VISIBLE
        splashScreen.loading = true
        
        tabs = findViewById(R.id.tabs)
        
        val actionBar = findViewById<Toolbar>(R.id.action_bar)
        setSupportActionBar(actionBar)
        
        //posts view pager
        postsViewPager = findViewById(R.id.posts_view_pager)
        postsViewPagerAdapter = PostsViewPagerAdapter(this)
        postsViewPager.adapter = postsViewPagerAdapter
        postsViewPager.isUserInputEnabled = false
        //val offscreenPageLimit = 1
        //postsViewPager.offscreenPageLimit = offscreenPageLimit
        
        /*val onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                val postsViewPagerAdapter = (postsViewPager.adapter as PostsViewPagerAdapter)
                //val lastOffscreenFragment = postsViewPagerAdapter.fragments[offscreenPageLimit]
                val currentFragment = postsViewPagerAdapter.fragments[position]

                fun onReady(currentFragment: PostsFragment) {
                    //Ensure all offscreen fragments loaded all their posts(implying that also the current displayed one)
                    if (currentFragment.isPostsLoading == null || currentFragment.isPostsLoading == true) {
                        currentFragment.onPostsLoadingEndListener = object : PostsFragment.OnPostsLoadingEndListener {
                            override fun onLoadingEnd(
                                status: Property.PropertyStatus,
                                success: Boolean,
                                startedAfterLastVisible: Boolean,
                                loadedCount: Int
                            ) {
                                onReady(currentFragment)
                            }
                        }
                    } else {
                        //nestedScrollView.stopNestedScroll()
                        nestedScrollView.fullScroll(View.FOCUS_UP)

                        val view = currentFragment.requireView()
                        //view.postDelayed({view.requestLayout()}, 2000)
                        val wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.width, View.MeasureSpec.EXACTLY)
                        val hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                        view.measure(wMeasureSpec, hMeasureSpec)
                        if (postsViewPager.layoutParams.height != view.measuredHeight) {
                            postsViewPager.layoutParams = (postsViewPager.layoutParams as LinearLayout.LayoutParams)
                                .also { lp -> lp.height = view.measuredHeight }
                        }
                    }
                }

                //Ensure all offscreen fragments have been created(implying that also the current displayed one)
                if (currentFragment != null) {
                    //val currentFragment = postsViewPagerAdapter.fragments[position]
                    onReady(currentFragment)
                } else {
                    val currentPosition = position
                    postsViewPagerAdapter.onFragmentCreatedListener = object : PostsViewPagerAdapter.OnFragmentCreatedListener {
                        override fun onFragmentCreated(position: Int, fragment: PostsFragment) {
                            if (position == currentPosition) {
                                //val currentFragment = postsViewPagerAdapter.fragments[position]
                                onReady(fragment)
                                postsViewPagerAdapter.onFragmentCreatedListener = null
                            }
                        }
                    }
                }

            }
        }*/
        //postsViewPager.registerOnPageChangeCallback(onPageChangeCallback)
        
        //TODO Same changes on Client's app
        val onViewPagerLayoutChangedListener =
            View.OnLayoutChangeListener { viewPager, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                val currentFragment = postsViewPagerAdapter.fragments[postsViewPager.currentItem]
                val oldHeight = oldBottom - oldTop
                if (viewPager.height != oldHeight)
                    currentFragment?.let { adjustViewPagerHeight(it) }
            }
        //Layout can also change without tab being switched(example : refresh)
        postsViewPager.addOnLayoutChangeListener(onViewPagerLayoutChangedListener)
        
        postsViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                PostsFragment.onSelectActionMode?.let {
                    it.finish()
                    PostsFragment.onSelectActionMode = null
                }
                /*val currentFragment = postsViewPagerAdapter.fragments[position]
                currentFragment?.recyclerViewAdapter?.clearSelection()*/
                
                nestedScrollView.fullScroll(View.FOCUS_UP)
                val currentFragment = postsViewPagerAdapter.fragments[position]
                
                currentFragment?.let { adjustViewPagerHeight(it) }
            }
        })
        
        TabLayoutMediator(tabs, postsViewPager) { tab, position ->
            tab.text = getString(Property.PropertyStatus.values()[position].stringRes)
        }.attach()
        
        
        //Navigation View settings
        drawerLayout = findViewById(R.id.drawer_layout)
        val navDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, actionBar, R.string.open_menu, R.string.close_menu)
        drawerLayout.addDrawerListener(navDrawerToggle)
        navDrawerToggle.syncState()
        val navigationView = findViewById<NavigationView>(R.id.navigation_view)
        navigationView.itemIconTintList = null
        navigationView.setNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.settings -> {
                }
                R.id.about -> {
                }
                R.id.share_app -> {
                }
                R.id.like_app -> {
                }
                R.id.rate_app -> {
                }
                R.id.exit -> {
                }
            }
            true
        }
        val drawerHeaderLayout = navigationView.getHeaderView(0)
        val newPostButtonDrawer = drawerHeaderLayout.findViewById<Button>(R.id.new_post_button_drawer)
        val switchToClientsButton = drawerHeaderLayout.findViewById<Button>(R.id.switch_to_clients_button)
        newPostButtonDrawer.setOnClickListener { newPost() }
        switchToClientsButton.setOnClickListener { switchToClients() }
        
        
        // New post fab apparition
        val newPostButton = findViewById<Button>(R.id.new_post_button)
        val newPostFab = findViewById<FloatingActionButton>(R.id.new_post_fab)
        val nestedAppBarLayout = findViewById<AppBarLayout>(R.id.nested_appbar_layout)
        nestedAppBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            val scrollBounds = Rect()
            nestedAppBarLayout.getHitRect(scrollBounds)
            // Any portion of the search button, even a single pixel, is within the visible window
            if (newPostButton.getLocalVisibleRect(scrollBounds)) newPostFab.hide() else newPostFab.show()
        })
        newPostButton.setOnClickListener { newPost() }
        newPostFab.setOnClickListener { newPost() }
    }
    
    private fun adjustViewPagerHeight(currentFragment: PostsFragment) {
        currentFragment.view?.let { view ->
            val wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.width, View.MeasureSpec.EXACTLY)
            val hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            view.measure(wMeasureSpec, hMeasureSpec)
            if (postsViewPager.height != view.measuredHeight) {
                postsViewPager.layoutParams = (postsViewPager.layoutParams as LinearLayout.LayoutParams)
                    .also { lp -> lp.height = view.measuredHeight }
            }
        }
    }
    
    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START)
        else super.onBackPressed()
    }
    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SIGN_IN_REQUEST_CODE) {
            handleSignInRequestResult(resultCode, data)
            
        } else if (requestCode == NEW_POST_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                data?.let {
                    val justAddedPropertyStatus = it.getSerializableExtra("status") as Property.PropertyStatus
                    postsViewPager.currentItem = justAddedPropertyStatus.ordinal
                    reloadPostsAt(justAddedPropertyStatus.ordinal)
                }
            }
            
        } else if (requestCode == COMPLETE_SIGN_UP_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                splashScreen.visibility = View.GONE
                //reloadPostsAt(postsViewPager.currentItem)
                
            } else if (resultCode == RESULT_CANCELED) finish()
        }
    }
    
    private fun reloadPostsAt(position: Int) {
        val postsViewPagerAdapter = (postsViewPager.adapter as PostsViewPagerAdapter)
        val currentFragment = postsViewPagerAdapter.fragments[postsViewPager.currentItem]
        currentFragment?.reloadPosts()
        if (currentFragment == null) {
            val fragmentPosition = position
            postsViewPagerAdapter.onFragmentCreatedListener = object :
                PostsViewPagerAdapter.OnFragmentCreatedListener {
                override fun onFragmentCreated(position: Int, fragment: PostsFragment) {
                    if (position == fragmentPosition) {
                        fragment.reloadPosts()
                        postsViewPagerAdapter.onFragmentCreatedListener = null
                    }
                }
            }
        }
    }
    
    private fun signOwnerIn() {
        //Check if user already logged in
        if (FirebaseAuth.getInstance().currentUser == null) {
            // Choose authentication providers
            val providers = listOf(
                EmailBuilder().setRequireName(false).build(),
                //new AuthUI.IdpConfig.PhoneBuilder().build(),
                GoogleBuilder().build()
                //new AuthUI.IdpConfig.FacebookBuilder().build(),
                //new AuthUI.IdpConfig.TwitterBuilder().build()
            )
            
            // Create and launch sign-in intent
            //TODO Terms Of Service and Privacy Policy Web page hosted on Firebase host product..
            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setTheme(R.style.LoginTheme)
                    .setLogo(R.drawable.immofind_colored_logo)
                    .setAlwaysShowSignInMethodScreen(true)
                    .setIsSmartLockEnabled(false)
                    .setAvailableProviders(providers)
                    .setTosAndPrivacyPolicyUrls(
                        getString(R.string.tosUrl),
                        getString(R.string.privacyUrl)
                    )
                    .build(),
                SIGN_IN_REQUEST_CODE
            )
        } else {
            requireOwnerCompleteRegistration(object : RequirementCallbacks {
                override fun doWhenRequirementFulfilled() {
                    splashScreen.loading = false
                    splashScreen.visibility = View.GONE
                    //reloadPostsAt(postsViewPager.currentItem)
                }
                
                override fun onRequirementFulfillingFailed(e: Exception) {
                    //TODO getErrorCodes from exception and check
                    e.printStackTrace()
                    Snackbar.make(drawerLayout, "Error", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry") { }
                        .show()
                }
            })
        }
    }
    
    private interface RequirementCallbacks {
        fun doWhenRequirementFulfilled()
        fun onRequirementFulfillingFailed(e: Exception)
    }
    
    private fun requireOwnerCompleteRegistration(requirementCallbacks: RequirementCallbacks) {
        val currentUser = FirebaseAuth.getInstance().currentUser
        com.ouedyan.immofind.owners.firestoreapi.OwnersHelper.getOwner(currentUser!!.uid)
            .addOnSuccessListener { documentSnapshot ->
                
                val ownerCompletedRegistration: Boolean
                if (documentSnapshot.exists()) {
                    if (documentSnapshot.contains("email"))
                        if (documentSnapshot["email"] == currentUser.email) ownerCompletedRegistration = true
                        else {
                            ownerCompletedRegistration = false
                            Timber.e("Current authenticated user email and Document email field do not match")
                        } else {
                        ownerCompletedRegistration = false
                        Timber.e("Document doesn't contain email field")
                    }
                } else {
                    ownerCompletedRegistration = false
                }
                if (ownerCompletedRegistration)
                    requirementCallbacks.doWhenRequirementFulfilled()
                else startActivityForResult(
                    Intent(this@MainActivityOwner, CompleteSignUpActivity::class.java),
                    COMPLETE_SIGN_UP_REQUEST_CODE
                )
            }
            .addOnFailureListener { e -> requirementCallbacks.onRequirementFulfillingFailed(e) }
    }
    
    private fun handleSignInRequestResult(resultCode: Int, data: Intent?) {
        val response = IdpResponse.fromResultIntent(data)
        if (resultCode == RESULT_OK) {
            //if new Owner, complete signing up
            requireOwnerCompleteRegistration(object : RequirementCallbacks {
                override fun doWhenRequirementFulfilled() {
                    splashScreen.loading = false
                    splashScreen.visibility = View.GONE
                    //reloadPostsAt(postsViewPager.currentItem)
                }
                
                override fun onRequirementFulfillingFailed(e: Exception) {
                    Snackbar.make(drawerLayout, "An error occurred", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry") { }
                        .show()
                    e.printStackTrace()
                }
            })
        } else { // ERRORS
            if (response == null) { // Cancelled authentication
                finish()
            } else {
                if (response.error!!.errorCode == ErrorCodes.NO_NETWORK)
                    Toast.makeText(this, "Network error. Check your internet connection.", Toast.LENGTH_SHORT).show()
                else if (response.error!!.errorCode == ErrorCodes.UNKNOWN_ERROR)
                    Toast.makeText(this, "An unknown error occurred.", Toast.LENGTH_SHORT).show()
                signOwnerIn()
            }
        }
    }
    
    
    private fun newPost() {
        startActivityForResult(Intent(this@MainActivityOwner, NewPostActivity::class.java), NEW_POST_REQUEST_CODE)
    }
    
    private fun switchToClients() {
        //TODO
        //finish()
    }
    
}

package com.ouedyan.immofind.owners.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.ouedyan.immofind.owners.R;
import com.ouedyan.immofind.owners.firestoreapi.OwnersHelper;
import com.ouedyan.immofind.owners.model.Owner;

import java.util.Objects;

import timber.log.Timber;


public class SignUpActivity extends AppCompatActivity {

    private EditText editDenomination,
            editFirstName,
            editLastName,
            editEmail,
            editPassword,
            editConfirmPassword;
    private ProgressBar signUpProgress;
    private ScrollView rootLayout;

    private FirebaseAuth mAuth;


    String denomination, firstName, lastName, email, password, confirmPassword;

    Boolean isBusiness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //View Binding
        rootLayout = findViewById(R.id.rootLayout);
        signUpProgress = findViewById(R.id.progress);
        RadioGroup radioIndividualOrBusiness = findViewById(R.id.radio_individual_or_business);
        editDenomination = findViewById(R.id.edit_denomination);
        editFirstName = findViewById(R.id.edit_first_name);
        editLastName = findViewById(R.id.edit_last_name);
        editEmail = findViewById(R.id.edit_txt_Email);
        editPassword = findViewById(R.id.edit_txt_Pass);
        editConfirmPassword = findViewById(R.id.edit_txt_CoPass);
        TextView textLogin = findViewById(R.id.text_view_login);
        Button buttonRegister = findViewById(R.id.button_register);

        //Get Firebase auth instance
        mAuth = FirebaseAuth.getInstance();

        //Displaying specific fields whether they are individuals or businesses
        radioIndividualOrBusiness = findViewById(R.id.radio_individual_or_business);
        radioIndividualOrBusiness.clearCheck();
        radioIndividualOrBusiness.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_individual) {
                    findViewById(R.id.field_denomination).setVisibility(View.GONE);
                    findViewById(R.id.field_first_name).setVisibility(View.VISIBLE);
                    findViewById(R.id.field_last_name).setVisibility(View.VISIBLE);
                    isBusiness = false;
                } else {
                    findViewById(R.id.field_denomination).setVisibility(View.VISIBLE);
                    findViewById(R.id.field_first_name).setVisibility(View.GONE);
                    findViewById(R.id.field_last_name).setVisibility(View.GONE);
                    isBusiness = true;
                }
            }
        });
        radioIndividualOrBusiness.check(R.id.radio_individual);//Individual checked by default


        textLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        //Handle user SignUp button
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateFields()) {
                    return;
                }
                //progressbar VISIBLE
                signUpProgress.setVisibility(View.VISIBLE);
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                Owner newOwner = new Owner();
                                newOwner.setBusiness(isBusiness);
                                newOwner.setDenomination(isBusiness ? denomination :
                                        firstName + " " + lastName);
                                newOwner.setEmail(email);

                                OwnersHelper.createOwner(newOwner,
                                        Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                signUpProgress.setVisibility(View.GONE);

                                                Toast.makeText(SignUpActivity.this, "Successfully Registered",
                                                        Toast.LENGTH_SHORT).show();
                                                setResult(RESULT_OK);
                                                finish();

                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                //Remove owner from firebase users
                                                mAuth.getCurrentUser().delete();
                                                signUpProgress.setVisibility(View.GONE);
                                                Snackbar.make(rootLayout, "Error while registering you to the database",
                                                        Snackbar.LENGTH_SHORT);
                                                Timber.e(e);

                                            }
                                        });

                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                signUpProgress.setVisibility(View.GONE);
                                Snackbar.make(rootLayout, "Error while registering you to the database",
                                        Snackbar.LENGTH_SHORT);
                                Timber.e(e);
                            }
                        });
            }
        });

    }

    private boolean validateFields() {
        boolean result = true;
        denomination = editDenomination.getText().toString().trim();
        firstName = editFirstName.getText().toString().trim();
        lastName = editLastName.getText().toString().trim();
        email = editEmail.getText().toString().trim();

        if (TextUtils.isEmpty(denomination)) {
            editDenomination.setError("Please enter your business' denomination");
            result = false;
        }

        if (TextUtils.isEmpty(firstName)) {
            editFirstName.setError("Please enter your first name");
            result = false;
        }

        if (TextUtils.isEmpty(lastName)) {
            editLastName.setError("Please enter your last name");
            result = false;
        }

        if (TextUtils.isEmpty(email)) {
            editEmail.setError("Please enter your email");
            result = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editEmail.setError("Please enter a valid email address");
            result = false;
        }

        if (TextUtils.isEmpty(password)) {
            editPassword.setError("Please enter your password");
            result = false;
        } else if (password.length() < 6) {
            editPassword.setError("Your password must be at least of 6 characters");
            result = false;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            editConfirmPassword.setError("Please confirm your password");
            result = false;
        } else if (!confirmPassword.equals(password)) {
            editConfirmPassword.setError("The entered passwords do not match");
            result = false;
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
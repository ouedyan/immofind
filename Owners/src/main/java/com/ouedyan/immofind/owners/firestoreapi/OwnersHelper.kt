package com.ouedyan.immofind.owners.firestoreapi

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.ouedyan.immofind.owners.model.Owner

object OwnersHelper {
    private const val COLLECTION_NAME = "owners"

    // --- COLLECTION REFERENCE ---
    val ownersCollection: CollectionReference
        get() = FirebaseFirestore.getInstance().collection(COLLECTION_NAME)

    // --- CREATE ---
    @JvmStatic
    fun createOwner(newOwner: Owner, authUid: String): Task<Void> {
        return ownersCollection.document(authUid).set(newOwner)
    }

    // --- GET ---
    fun getOwner(uid: String): Task<DocumentSnapshot> {
        return ownersCollection.document(uid).get()
    }

    // --- UPDATE ---
    fun updateOwner(ownerNewData: Owner): Task<Void> {
        return ownersCollection.document(ownerNewData.uid).set(ownerNewData)
    }

    // --- DELETE ---
    fun deleteOwner(uid: String): Task<Void> {
        return ownersCollection.document(uid).delete()
    }
}
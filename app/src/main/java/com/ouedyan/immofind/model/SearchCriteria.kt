package com.ouedyan.immofind.model

import androidx.annotation.StringRes
import com.ouedyan.immofind.R
import com.ouedyan.immofind.model.Property.TypeOfProperty


interface SearchCriteria {
    var city: String
    var isRented: Boolean
    var budgetMin: Double
    var budgetMax: Double
    val typesOfPropertyCriteria : HashSet<TypeOfPropertyCriteria>


    enum class HabitationSearchSpecificity(@field:StringRes @param:StringRes val stringRes: Int) {
        FURNISHED(R.string.furnished),
        ACCESS_TO_ELECTRICITY(R.string.access_to_electricity),
        ACCESS_TO_WATER(R.string.access_to_water),
        AIR_CONDITIONED(R.string.air_conditioned),
        FENCED(R.string.fenced),
        GARAGE(R.string.garage),
        SWIMMING_POOL(R.string.swimming_pool)
    }

    enum class LandOrFarmSearchSpecificity(@field:StringRes @param:StringRes val stringRes: Int) {
        ACCESS_TO_ELECTRICITY(R.string.access_to_electricity),
        ACCESS_TO_WATER(R.string.access_to_water),
        FENCED(R.string.fenced)
    }

    enum class BuildingSearchSpecificity(@field:StringRes @param:StringRes val stringRes: Int) {
        FENCED(R.string.fenced),
        PARKING(R.string.parking),
        GARAGE(R.string.garage)
    }

    abstract class TypeOfPropertyCriteria {
        abstract val typesOfProperty: HashSet<TypeOfProperty>

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || this::class.qualifiedName != other::class.qualifiedName) return false
            return typesOfProperty == (other as TypeOfPropertyCriteria).typesOfProperty
        }

        override fun hashCode(): Int {
            return typesOfProperty.hashCode()
        }

    }

    abstract class HabitationCriteria : TypeOfPropertyCriteria() {
        final override val typesOfProperty = HashSet(listOf(TypeOfProperty.HABITATION))
        abstract val surfaceMin: Double
        abstract val surfaceMax: Double
        abstract val landSurfaceMin: Double
        abstract val landSurfaceMax: Double
        abstract val numberOfRooms: Int
        abstract val numberOfBedrooms: Int
        abstract val searchSpecificity: HashSet<HabitationSearchSpecificity>
    }

    abstract class LandOrFarmCriteria : TypeOfPropertyCriteria() {
        final override val typesOfProperty = HashSet(listOf(TypeOfProperty.LAND_OR_LAND_LOT, TypeOfProperty.FARM))
        abstract val landSurfaceMin: Double
        abstract val landSurfaceMax: Double
        abstract val searchSpecificity: HashSet<LandOrFarmSearchSpecificity>
    }

    abstract class BuildingCriteria : TypeOfPropertyCriteria() {
        final override val typesOfProperty = HashSet(listOf(TypeOfProperty.BUILDING))
        abstract val surfaceMin: Double
        abstract val surfaceMax: Double
        abstract val numberOfRoomsMin: Int
        abstract val numberOfRoomsMax: Int
        abstract val searchSpecificity: HashSet<BuildingSearchSpecificity>
    }

    abstract class OfficeRoomsCriteria : TypeOfPropertyCriteria() {
        final override val typesOfProperty = HashSet(listOf(TypeOfProperty.OFFICE_ROOMS))
        abstract val numberOfOfficeRoomsMin: Int
        abstract val numberOfOfficeRoomsMax: Int
    }
}

//Just for tests
fun tries(){
    val searchCriteria = object : SearchCriteria{
        override var city: String
            get() = TODO("Not yet implemented")
            set(value) {}
        override var isRented: Boolean
            get() = TODO("Not yet implemented")
            set(value) {}
        override var budgetMin: Double
            get() = TODO("Not yet implemented")
            set(value) {}
        override var budgetMax: Double
            get() = TODO("Not yet implemented")
            set(value) {}
        override val typesOfPropertyCriteria: HashSet<SearchCriteria.TypeOfPropertyCriteria>
            get() = TODO("Not yet implemented")

    }

    searchCriteria.city
    searchCriteria.isRented
    searchCriteria.budgetMax
    searchCriteria.budgetMin
    searchCriteria.typesOfPropertyCriteria.forEach {
        it.typesOfProperty.forEach { typeOfProperty ->
            when(typeOfProperty){
                TypeOfProperty.HABITATION -> TODO()
                TypeOfProperty.LAND_OR_LAND_LOT -> TODO()
                TypeOfProperty.BUILDING -> TODO()
                TypeOfProperty.COMMERCIAL_PLACE -> TODO()
                TypeOfProperty.OFFICE_ROOMS -> TODO()
                TypeOfProperty.STORE -> TODO()
                TypeOfProperty.FARM -> TODO()
                TypeOfProperty.FACTORY -> TODO()
            }
        }
    }
}
package com.ouedyan.immofind.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler
import android.os.Looper

import com.ouedyan.immofind.R;

class WelcomeScreenActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.splash_screen)

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent (this@WelcomeScreenActivity, MainActivityClient::class.java))
            finish()
        }, 1000)
    }
}
package com.ouedyan.immofind.ui

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.widget.NestedScrollView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.ouedyan.immofind.PostsFragment
import com.ouedyan.immofind.PostsViewPagerAdapter
import com.ouedyan.immofind.R
import com.ouedyan.immofind.model.Property
import java.util.*

class MainActivityClient : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var nestedScrollView: NestedScrollView
    private lateinit var tabs: TabLayout
    
    private lateinit var postsViewPager: ViewPager2
    private lateinit var postsViewPagerAdapter: PostsViewPagerAdapter
    
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main_client)
        nestedScrollView = findViewById(R.id.nested_scrollview)
    
        tabs = findViewById(R.id.tabs)
    
        val actionBar = findViewById<Toolbar>(R.id.action_bar)
        setSupportActionBar(actionBar)
    
        //posts view pager
        postsViewPager = findViewById(R.id.posts_view_pager)
        postsViewPagerAdapter = PostsViewPagerAdapter(this)
        postsViewPager.adapter = postsViewPagerAdapter
        postsViewPager.isUserInputEnabled = false
        //val offscreenPageLimit = 1
        //postsViewPager.offscreenPageLimit = offscreenPageLimit
    
        /*val onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                val postsViewPagerAdapter = (postsViewPager.adapter as PostsViewPagerAdapter)
                //val lastOffscreenFragment = postsViewPagerAdapter.fragments[offscreenPageLimit]
                val currentFragment = postsViewPagerAdapter.fragments[position]

                fun onReady(currentFragment: PostsFragment) {
                    //Ensure all offscreen fragments loaded all their posts(implying that also the current displayed one)
                    if (currentFragment.isPostsLoading == null || currentFragment.isPostsLoading == true) {
                        currentFragment.onPostsLoadingEndListener = object : PostsFragment.OnPostsLoadingEndListener {
                            override fun onLoadingEnd(
                                status: Property.PropertyStatus,
                                success: Boolean,
                                startedAfterLastVisible: Boolean,
                                loadedCount: Int
                            ) {
                                onReady(currentFragment)
                            }
                        }
                    } else {
                        //nestedScrollView.stopNestedScroll()
                        nestedScrollView.fullScroll(View.FOCUS_UP)

                        val view = currentFragment.requireView()
                        //view.postDelayed({view.requestLayout()}, 2000)
                        val wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.width, View.MeasureSpec.EXACTLY)
                        val hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                        view.measure(wMeasureSpec, hMeasureSpec)
                        if (postsViewPager.layoutParams.height != view.measuredHeight) {
                            postsViewPager.layoutParams = (postsViewPager.layoutParams as LinearLayout.LayoutParams)
                                .also { lp -> lp.height = view.measuredHeight }
                        }
                    }
                }

                //Ensure all offscreen fragments have been created(implying that also the current displayed one)
                if (currentFragment != null) {
                    //val currentFragment = postsViewPagerAdapter.fragments[position]
                    onReady(currentFragment)
                } else {
                    val currentPosition = position
                    postsViewPagerAdapter.onFragmentCreatedListener = object : PostsViewPagerAdapter.OnFragmentCreatedListener {
                        override fun onFragmentCreated(position: Int, fragment: PostsFragment) {
                            if (position == currentPosition) {
                                //val currentFragment = postsViewPagerAdapter.fragments[position]
                                onReady(fragment)
                                postsViewPagerAdapter.onFragmentCreatedListener = null
                            }
                        }
                    }
                }

            }
        }*/
        //postsViewPager.registerOnPageChangeCallback(onPageChangeCallback)
    
        val onViewPagerLayoutChangedListener =
            View.OnLayoutChangeListener { viewPager, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                val currentFragment = postsViewPagerAdapter.fragments[postsViewPager.currentItem]
                val oldHeight = oldBottom - oldTop
                if (viewPager.height != oldHeight)
                    currentFragment?.let { adjustViewPagerHeight(it) }
            }
        //Layout can also change without tab being switched(example : refresh)
        postsViewPager.addOnLayoutChangeListener(onViewPagerLayoutChangedListener)
    
        postsViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                /*val currentFragment = postsViewPagerAdapter.fragments[position]
                currentFragment?.recyclerViewAdapter?.clearSelection()*/
            
                nestedScrollView.fullScroll(View.FOCUS_UP)
                val currentFragment = postsViewPagerAdapter.fragments[position]
            
                currentFragment?.let { adjustViewPagerHeight(it) }
            }
        })
    
        TabLayoutMediator(tabs, postsViewPager) { tab, position ->
            tab.text = getString(Property.PropertyStatus.values()[position].stringRes)
        }.attach()
    
        
        //Navigation View settings
        drawerLayout = findViewById(R.id.drawer_layout)
        val navDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, actionBar, R.string.open_menu, R.string.close_menu)
        drawerLayout.addDrawerListener(navDrawerToggle)
        navDrawerToggle.syncState()
        val navigationView = findViewById<NavigationView>(R.id.navigation_view)
        navigationView.itemIconTintList = null
        navigationView.setNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.saved -> {
                }
                R.id.settings -> {
                }
                R.id.about -> {
                }
                R.id.share_app -> {
                }
                R.id.like_app -> {
                }
                R.id.rate_app -> {
                }
                R.id.exit -> {
                }
            }
            true
        }
        val drawerHeaderLayout = navigationView.getHeaderView(0)
        val newSearchButtonDrawer = drawerHeaderLayout.findViewById<Button>(R.id.new_search_button_drawer)
        val switchToOwnersButton = drawerHeaderLayout.findViewById<Button>(R.id.switch_to_owners_button)
        newSearchButtonDrawer.setOnClickListener { newSearch() }
        switchToOwnersButton.setOnClickListener { switchToOwners() }


        // Search fab apparition
        val searchButton = findViewById<Button>(R.id.search_button)
        val searchFab = findViewById<FloatingActionButton>(R.id.search_fab)
        val nestedAppBarLayout = findViewById<AppBarLayout>(R.id.nested_appbar_layout)
        nestedAppBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            val scrollBounds = Rect()
            nestedAppBarLayout.getHitRect(scrollBounds)
            // Any portion of the search button, even a single pixel, is within the visible window
            if (searchButton.getLocalVisibleRect(scrollBounds)) searchFab.hide() else searchFab.show()
        })
        searchButton.setOnClickListener { newSearch() }
        searchFab.setOnClickListener { newSearch() }
    }
    
    private fun adjustViewPagerHeight(currentFragment: PostsFragment) {
        currentFragment.view?.let { view ->
            val wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.width, View.MeasureSpec.EXACTLY)
            val hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            view.measure(wMeasureSpec, hMeasureSpec)
            if (postsViewPager.height != view.measuredHeight) {
                postsViewPager.layoutParams = (postsViewPager.layoutParams as LinearLayout.LayoutParams)
                    .also { lp -> lp.height = view.measuredHeight }
            }
        }
    }
    
    
    
    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START)
        else super.onBackPressed()
    }


    private fun newSearch() {
        startActivity(Intent(this@MainActivityClient, SearchActivity::class.java))
    }

    private fun switchToOwners() {
        //TODO Remove
        //finish()
    }
}
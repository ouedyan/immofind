package com.ouedyan.immofind.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.chivorn.smartmaterialspinner.SmartMaterialSpinner
import com.google.android.material.slider.RangeSlider
import com.ouedyan.immofind.model.City
import com.ouedyan.immofind.Location
import com.ouedyan.immofind.R
import it.sephiroth.android.library.numberpicker.NumberPicker
import it.sephiroth.android.library.numberpicker.doOnProgressChanged

class SearchActivity : AppCompatActivity() {
    //TODO Get country from settings
    private val currentCountry = "BF"

    private lateinit var radioGroupStatus: RadioGroup
    private lateinit var locationInCity: AutoCompleteTextView
    private lateinit var citySpinner: SmartMaterialSpinner<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val res = resources

        //City spinner setup
        citySpinner = findViewById(R.id.spinner_city)
        setupCitySpinner()

        //Enabling location in city auto complete editText according to city is selected or not
        locationInCity = findViewById(R.id.auto_complete_location_in_city)
        setupLocationInCityEditText()

        //Habitation room comfort setup
        val habitationRoomComfort = findViewById<MultiQuantifiedToggleLayout>(R.id.habitation_room_comfort)
        habitationRoomComfort.apply {
            addSimpleToggle("1",
                res.getString(R.string.all),
                res.getString(R.string.all),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("2",
                "1",
                "1",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("3",
                "2",
                "2",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("4",
                "3",
                "3",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("5",
                "4",
                "4",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("6",
                "5+",
                "5+",
                onCheckedChangeListener = { toggle, isChecked ->

                })

        }

        //Habitation bedroom comfort setup
        val habitationBedroomComfort = findViewById<MultiQuantifiedToggleLayout>(R.id.habitation_bedroom_comfort)
        habitationBedroomComfort.apply {
            addSimpleToggle("1",
                res.getString(R.string.all),
                res.getString(R.string.all),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("2",
                "1",
                "1",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("3",
                "2",
                "2",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("4",
                "3",
                "3",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("5",
                "4",
                "4",
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("6",
                "5+",
                "5+",
                onCheckedChangeListener = { toggle, isChecked ->

                })

        }

        //Habitation block specificities setup
        val habitationSpecificities = findViewById<MultiQuantifiedToggleLayout>(R.id.habitation_specificities)
        habitationSpecificities.apply {
            addSimpleToggle("1",
                res.getString(R.string.furnished),
                res.getString(R.string.furnished),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("2",
                res.getString(R.string.access_to_electricity),
                res.getString(R.string.access_to_electricity),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("3",
                res.getString(R.string.access_to_water),
                res.getString(R.string.access_to_water),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("4",
                res.getString(R.string.fenced),
                res.getString(R.string.fenced),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("5",
                res.getString(R.string.air_conditioned),
                res.getString(R.string.air_conditioned),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("6",
                res.getString(R.string.garage),
                res.getString(R.string.garage),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("7",
                res.getString(R.string.swimming_pool),
                res.getString(R.string.swimming_pool),
                onCheckedChangeListener = { toggle, isChecked ->

                })
        }

        //Land/Farm block specificities setup
        val landOrFarmSpecificities = findViewById<MultiQuantifiedToggleLayout>(R.id.land_or_farm_specificities)
        landOrFarmSpecificities.apply {
            addSimpleToggle("1",
                res.getString(R.string.access_to_electricity),
                res.getString(R.string.access_to_electricity),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("2",
                res.getString(R.string.access_to_water),
                res.getString(R.string.access_to_water),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("3",
                res.getString(R.string.fenced),
                res.getString(R.string.fenced),
                onCheckedChangeListener = { toggle, isChecked ->

                })

        }

        //Building block specificities setup
        val buildingSpecificities = findViewById<MultiQuantifiedToggleLayout>(R.id.building_specificities)
        buildingSpecificities.apply {
            addSimpleToggle("1",
                res.getString(R.string.fenced),
                res.getString(R.string.fenced),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("2",
                res.getString(R.string.parking),
                res.getString(R.string.parking),
                onCheckedChangeListener = { toggle, isChecked ->

                })
            addSimpleToggle("3",
                res.getString(R.string.garage),
                res.getString(R.string.garage),
                onCheckedChangeListener = { toggle, isChecked ->

                })

        }


        //Min and max values of type of properties' characteristics setting in accordance to selected
        // status(for rent or sale)
        radioGroupStatus = findViewById(R.id.radio_rent_or_sale)
        radioGroupStatus.clearCheck()
        radioGroupStatus.check(R.id.radio_rent) // Check for rent by default


        //TODO Don't forget "Location in City : neighborhood, sector, or other...". Not implemented yet
        linkRangeSlidersAndNumberPickers()
    }

    private fun setupCitySpinner() {
        val cities = ArrayList<City>()
        Location.getCountry(currentCountry)?.let { cities.addAll(it.cities) }
        val citiesNames = ArrayList<String>()
        for (city in cities) citiesNames.add(city.name)
        citySpinner.item = citiesNames
    }

    private fun setupLocationInCityEditText() {
        val cities = ArrayList<City>()
        Location.getCountry(currentCountry)?.let { cities.addAll(it.cities) }
        locationInCity.inputType = EditorInfo.TYPE_NULL
        citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                locationInCity.apply {
                    isEnabled = true
                    text = null
                    inputType = EditorInfo.TYPE_TEXT_VARIATION_SHORT_MESSAGE
                    inputType = EditorInfo.TYPE_TEXT_FLAG_CAP_WORDS
                    val sectors = ArrayList<String>()
                    cities[position].numberOfSectors?.let {
                        if (it > 1)
                            for (i in 1..it) sectors.add(getString(R.string.sector) + " $i")
                    }
                    val neighborhoods = ArrayList<String>()
                    cities[position].neighborhoods?.let { neighborhoods += it }
                    setAdapter(
                        ArrayAdapter(
                            this@SearchActivity,
                            android.R.layout.simple_dropdown_item_1line,
                            (neighborhoods + sectors)
                        )
                    )
                    requestFocus()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                locationInCity.text = null
                locationInCity.inputType = EditorInfo.TYPE_NULL
                locationInCity.isEnabled = false
            }
        }

        //Clearing focus from location in city auto complete editText after a suggestion has been selected
        //(note : OnItemSelectedListener doesn't work)
        locationInCity.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            locationInCity.onEditorAction(EditorInfo.IME_ACTION_DONE)
        }
    }


    private fun setupLinkedRangeSliderAndNumberPickersDefaultValues(
        rangeSlider: RangeSlider,
        numberPickerMin: NumberPicker,
        numberPickerMax: NumberPicker,
        minValue: Int,
        maxValue: Int,
        minDefaultValue: Int,
        maxDefaultValue: Int
    ) {
        numberPickerMin.maxValue = maxValue.toDouble()
        numberPickerMin.minValue = minValue.toDouble()
        numberPickerMin.setProgress(minDefaultValue.toBigDecimal())
        numberPickerMax.maxValue = maxValue.toDouble()
        numberPickerMax.minValue = minValue.toDouble()
        numberPickerMax.setProgress(maxDefaultValue.toBigDecimal())
        rangeSlider.valueFrom = minValue.toFloat()
        rangeSlider.valueTo = maxValue.toFloat()
        rangeSlider.setValues(minDefaultValue.toFloat(), maxDefaultValue.toFloat())
    }

    private fun linkRangeSlidersAndNumberPickers() {
        //Budget range slider
        val numberPickerMinBudget = findViewById<NumberPicker>(R.id.number_picker_min_budget)
        val numberPickerMaxBudget = findViewById<NumberPicker>(R.id.number_picker_max_budget)
        linkRangeNumberPickers(numberPickerMinBudget, numberPickerMaxBudget)

        //Habitation surface range slider
        val numberPickerMinSurfaceHabitation = findViewById<NumberPicker>(R.id.number_picker_min_surface_habitation)
        val numberPickerMaxSurfaceHabitation = findViewById<NumberPicker>(R.id.number_picker_max_surface_habitation)
        val rangeSliderSurfaceHabitation = findViewById<RangeSlider>(R.id.range_slider_surface_habitation)
        linkRangeSliderAndNumberPickers(
            rangeSliderSurfaceHabitation,
            numberPickerMinSurfaceHabitation,
            numberPickerMaxSurfaceHabitation
        )

        //Habitation land surface range slider
        val numberPickerMinLandSurfaceHabitation = findViewById<NumberPicker>(R.id.number_picker_min_land_surface_habitation)
        val numberPickerMaxLandSurfaceHabitation = findViewById<NumberPicker>(R.id.number_picker_max_land_surface_habitation)
        val rangeSliderLandSurfaceHabitation = findViewById<RangeSlider>(R.id.range_slider_land_surface_habitation)
        linkRangeSliderAndNumberPickers(
            rangeSliderLandSurfaceHabitation,
            numberPickerMinLandSurfaceHabitation,
            numberPickerMaxLandSurfaceHabitation
        )

        //Land/Farm land surface range slider
        val numberPickerMinLandSurfaceLandOrFarm = findViewById<NumberPicker>(R.id.number_picker_min_land_surface_land_or_farm)
        val numberPickerMaxLandSurfaceLandOrFarm = findViewById<NumberPicker>(R.id.number_picker_max_land_surface_land_or_farm)
        val rangeSliderLandSurfaceLandOrFarm = findViewById<RangeSlider>(R.id.range_slider_land_surface_land_or_farm)
        linkRangeSliderAndNumberPickers(
            rangeSliderLandSurfaceLandOrFarm,
            numberPickerMinLandSurfaceLandOrFarm,
            numberPickerMaxLandSurfaceLandOrFarm
        )

        //Building surface range slider
        val numberPickerMinSurfaceBuilding = findViewById<NumberPicker>(R.id.number_picker_min_surface_building)
        val numberPickerMaxSurfaceBuilding = findViewById<NumberPicker>(R.id.number_picker_max_surface_building)
        val rangeSliderSurfaceBuilding = findViewById<RangeSlider>(R.id.range_slider_surface_building)
        linkRangeSliderAndNumberPickers(
            rangeSliderSurfaceBuilding,
            numberPickerMinSurfaceBuilding,
            numberPickerMaxSurfaceBuilding
        )

        //Building rooms range slider
        val numberPickerMinRoomsBuilding = findViewById<NumberPicker>(R.id.number_picker_min_rooms_building)
        val numberPickerMaxRoomsBuilding = findViewById<NumberPicker>(R.id.number_picker_max_rooms_building)
        val rangeSliderRoomsBuilding = findViewById<RangeSlider>(R.id.range_slider_rooms_building)
        linkRangeSliderAndNumberPickers(rangeSliderRoomsBuilding, numberPickerMinRoomsBuilding, numberPickerMaxRoomsBuilding)

        //Office rooms range slider
        val numberPickerMinOfficeRooms = findViewById<NumberPicker>(R.id.number_picker_min_office_rooms_office_rooms)
        val numberPickerMaxOfficeRooms = findViewById<NumberPicker>(R.id.number_picker_max_office_rooms_office_rooms)
        val rangeSliderOfficeRooms = findViewById<RangeSlider>(R.id.range_slider_office_rooms_office_rooms)
        linkRangeSliderAndNumberPickers(rangeSliderOfficeRooms, numberPickerMinOfficeRooms, numberPickerMaxOfficeRooms)
    }

    private fun linkRangeNumberPickers(numberPickerMin: NumberPicker, numberPickerMax: NumberPicker) {
        numberPickerMin.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            if (progress > numberPickerMax.getProgress()) numberPickerMax.setProgress(progress)
        }

        numberPickerMax.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            if (progress < numberPickerMin.getProgress()) numberPickerMin.setProgress(progress)
        }

    }

    private fun linkRangeSliderAndNumberPickers(
        rangeSlider: RangeSlider,
        numberPickerMin: NumberPicker,
        numberPickerMax: NumberPicker
    ) {
        numberPickerMin.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            rangeSlider.setValues(progress.toFloat(), rangeSlider.values[1])
        }

        numberPickerMax.doOnProgressChanged { thisNumberPicker, progress, formUser ->
            rangeSlider.setValues(rangeSlider.values[0], progress.toFloat())
        }

        rangeSlider.addOnChangeListener { slider, value, fromUser ->
            numberPickerMin.setProgress(slider.values[0].toBigDecimal().stripTrailingZeros())
            numberPickerMax.setProgress(slider.values[1].toBigDecimal().stripTrailingZeros())

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun search(view: View?) {}

    fun onTypeOfPropertyToggle(view: View?) {
        //Check if at least one type of property selected
        val allTogglesIds = listOf(
            R.id.toggle_habitation,
            R.id.toggle_land,
            R.id.toggle_farm,
            R.id.toggle_building,
            R.id.toggle_office_rooms,
            R.id.toggle_factory,
            R.id.toggle_commercial_place,
            R.id.toggle_store
        )
        var atLeastOneChecked = false
        for (blockId in allTogglesIds)
            if (findViewById<ToggleButton>(blockId).isChecked) {
                atLeastOneChecked = true
                break
            }

        if (!atLeastOneChecked) {
            if (view != null)
                findViewById<ToggleButton>(view.id).isChecked = true
            Toast.makeText(
                applicationContext, "You must choose at least one type of property", Toast
                    .LENGTH_SHORT
            ).show()
        }

        val selectedBlockId: Int? = when ((view as ToggleButton).id) {
            R.id.toggle_habitation -> R.id.habitation_block
            R.id.toggle_land,
            R.id.toggle_farm -> R.id.land_or_farm_block
            R.id.toggle_building -> R.id.building_block
            R.id.toggle_office_rooms -> R.id.office_rooms_block
            R.id.toggle_factory,
            R.id.toggle_commercial_place,
            R.id.toggle_store -> null
            else -> null
        }
        selectedBlockId?.let { findViewById<View>(it).isVisible = view.isChecked }
    }

    fun onHabitationRoomComfortToggle(view: View?) {}
    fun onHabitationBedroomComfortToggle(view: View?) {}
    fun onHabitationSpecificityToggle(view: View?) {}
    fun onLandOrFarmSpecificityToggle(view: View?) {}
    fun onBuildingSpecificityToggle(view: View?) {}
}
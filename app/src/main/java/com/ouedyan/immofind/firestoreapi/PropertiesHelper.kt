package com.ouedyan.immofind.firestoreapi

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.ouedyan.immofind.model.Property

object PropertiesHelper {
    private const val COLLECTION_NAME = "properties"

    // --- COLLECTION REFERENCE ---
    val propertiesCollection: CollectionReference
        get() = FirebaseFirestore.getInstance().collection(COLLECTION_NAME)

    // --- GET ---
    fun getProperty(uid: String): Task<DocumentSnapshot> {
        return propertiesCollection.document(uid).get()
    }

    fun getProperties(status : Property.PropertyStatus, startAfter : DocumentSnapshot? = null) : Task<QuerySnapshot> {
        val query = propertiesCollection
            .whereEqualTo("status", status)
            .orderBy("postDate", Query.Direction.DESCENDING)
            .limit(15)
        startAfter?.let { query.startAfter(it) }

        return query.get()
    }

}
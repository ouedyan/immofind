package com.ouedyan.immofind

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.DocumentSnapshot
import com.ouedyan.immofind.firestoreapi.PropertiesHelper
import com.ouedyan.immofind.model.Property
import com.scwang.smart.refresh.layout.SmartRefreshLayout

class PostsFragment(val status: Property.PropertyStatus) : Fragment() {
    lateinit var recyclerView: RecyclerView
    lateinit var smartRefreshLayout: SmartRefreshLayout
    var onPostsLoadingEndListener : OnPostsLoadingEndListener? = null

    /**
     * @return *null* if no loading was yet done
     */
    var isPostsLoading : Boolean? = null
    var onFragmentViewCreatedListener : OnFragmentViewCreatedListener? = null

    private val properties = ArrayList<Property>()
    private var lastVisibleProperty: DocumentSnapshot? = null

    interface OnFragmentViewCreatedListener{
        fun onViewCreated(view: View)
    }

    interface OnPostsLoadingEndListener{
        fun onLoadingEnd(status: Property.PropertyStatus, success : Boolean, startedAfterLastVisible: Boolean, loadedCount : Int)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val layout = inflater.inflate(R.layout.posts_fragment_layout, container, false)

        recyclerView = layout.findViewById(R.id.posts_recycler_view)
        smartRefreshLayout = layout.findViewById(R.id.posts_smart_refresh)

        //posts adapter
        val propertyAdapter = PropertyAdapter(properties, inflater.context)
        recyclerView.adapter = propertyAdapter

        smartRefreshLayout.setOnRefreshListener {
            reloadPosts()
        }
        
        //First
        loadPosts(status)

        return layout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onFragmentViewCreatedListener?.onViewCreated(view)

    }

    fun reloadPosts(){
        loadPosts(status)
    }

    private fun loadPosts(status: Property.PropertyStatus, startAfterLastVisible: Boolean = false) {
        isPostsLoading = true
        PropertiesHelper.getProperties(status, if (startAfterLastVisible) lastVisibleProperty else null)
            .addOnSuccessListener { documentSnapshots ->
                if (!startAfterLastVisible) {
                    properties.clear()
                    lastVisibleProperty = null
                    (recyclerView.adapter)?.notifyDataSetChanged()
                }
                var loadedCount = 0
                if (!documentSnapshots.isEmpty) {
                    documentSnapshots.forEach {
                        properties.add(it.toObject(Property::class.java))
                        (recyclerView.adapter)?.notifyItemInserted(properties.size - 1)
                        loadedCount++
                    }
                    lastVisibleProperty = documentSnapshots.documents[documentSnapshots.size() - 1]

                }
                smartRefreshLayout.finishRefresh()
                isPostsLoading = false
                onPostsLoadingEndListener?.onLoadingEnd(status, true, startAfterLastVisible, loadedCount)
            }
            .addOnFailureListener { e ->
                Log.e("loadFirstPosts", e.stackTraceToString())
                view?.let {
                    Snackbar.make(it, "Error while retrieving posts", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry") { loadPosts(status) }
                        .show()
                }
                smartRefreshLayout.finishRefresh()
                isPostsLoading = false
                onPostsLoadingEndListener?.onLoadingEnd(status, false, startAfterLastVisible, 0)
            }
    }

}